﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using DataAccess.Models;
using DataAccess.Models.Base;
using ManagerApiClient.Models;
using Newtonsoft.Json.Linq;
using Utilities;

namespace ManagerApiClient
{
    public class ConsoleManager
    {
        private readonly Executer _executer;
        private const string BaseUrl = "http://localhost:6996/api";
        private User _me;
        private Movie _selectedMovie;

        public ConsoleManager(Executer executer)
        {
            _executer = executer;
        }

        private static string ReadLine(string display)
        {
            Console.Write($"\n{display} ");
            return Console.ReadLine();
        }

        private static void ReadKey()
        {
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public void Execute()
        {
            DoLogin();
            DisplayMenu();
        }

        /// <summary>
        /// Runs the method that reads an object data from the console, and then calls the service which creates the object in the database.
        /// </summary>
        /// <typeparam name="T">Represents a class (any, usually derived from ModelBase or an IEnumerable), from which objects can be created (hence the new() constraint)</typeparam>
        /// <param name="path">Represents the RELATIVE path of the API where the resource being created resides.</param>
        /// <param name="middleWare">The action to execute between creating the object from console and sending the object to the server.</param>
        /// <returns></returns>
        public T CreateObjectProcess<T>(string path, Action<T> middleWare = null)
            where T: class, new()
        {
            var created = _executer.ExecuteMethod(new Func<T>(CreateObjectFromInput<T>)) as T;
            if (created == null)
            {
                Console.WriteLine("Cannot create " + typeof(T).Name + ". Please try again.");
                return null;
            }

            middleWare?.Invoke(created);// run additional middle tasks, like adding parameters

            _executer.ExecuteMethod(
                new Func<string, string, bool, object, T>(DoWorkAndGetServerResponse<T>),
                path, "POST", true, created);
            return created;
        }

        /// <summary>
        /// Runs the method that reads an object data from the console, and then calls the service which creates the object in the database.
        /// </summary>
        /// <typeparam name="T">Represents a class (any, usually derived from ModelBase or an IEnumerable), from which objects can be created (hence the new() constraint)</typeparam>
        /// <param name="path">Represents the RELATIVE path of the API where the resource being created resides.</param>
        /// <returns></returns>
        public T GetObjectProcess<T>(string path)
            where T : class
        {
            return _executer.ExecuteMethod(
                new Func<string, string, bool, object, T>(DoWorkAndGetServerResponse<T>),
                path, "GET", false, null) as T;
        }

        /// <summary>
        /// Reads all the required properties of an object from the console, builds the object and returns it.
        /// </summary>
        /// <typeparam name="T">Represents a class (any, usually derived from ModelBase or an IEnumerable), from which objects can be created (hence the new() constraint)</typeparam>
        /// <returns>T object</returns>
        public T CreateObjectFromInput<T>()
            where T : class, new()
        {
            Console.WriteLine("Insert data to create an " + typeof(T).Name + ": ");
            var created = new T();
            try
            {
                var modelBase = created as ModelBase;
                if (modelBase != null) modelBase.Id = Guid.NewGuid();
            }
            catch
            {
                // ignored
            }
            try
            {
                foreach (var prop in typeof(T).GetProperties())
                {
                    if ((prop.GetValue(created) != null && prop.Name != "ReleaseDate" && prop.Name != "Value") || prop.PropertyType.BaseType == typeof(ModelBase) || (prop.Name.Contains("Id") && prop.Name != "ImdbId")) continue;// skip default value props (special case release date for movie)

                    var input = ReadLine(prop.Name + ": ");
                    prop.SetValue(created, Convert.ChangeType(input, prop.PropertyType), null);
                }
            }
            catch
            {
                return null;
            }

            return created;
        }
        
        /// <summary>
        /// Calls the WCF service on the specified path, with the specified method, uploads data if needed and gets the response of the server (throws Exception if the server returns an error code). It reads the response from the server as an ServerResponse object, and then, if it exists, deserializes the data fron the .Data component.
        /// </summary>
        /// <returns>The object returned from the serever in the "Data" component.</returns>
        private static T DoWorkAndGetServerResponse<T>(string path, string method = "GET", bool upData = false, object data = null)
            where T: class
        {
            var client = new WebClient();
            var serviceUrl = $"{BaseUrl}/{path}";
            byte[] responseData;

            if (upData)
            {
                client.Headers["Content-type"] = "text/plain";
                var ser = JObject.FromObject(data).ToString();
                var barr = Encoding.UTF8.GetBytes(ser);

                // invoke the REST method  
                responseData = client.UploadData(serviceUrl, method, barr);
            }
            else
            {
                responseData = client.DownloadData(serviceUrl);
            }
            
            var stream = new MemoryStream(responseData);
            var responseDeserializer = new DataContractJsonSerializer(typeof(ServerResponse));
            var response = responseDeserializer.ReadObject(stream) as ServerResponse;
            if(response == null || response.StatusCode == 0)
                throw new Exception("Error came from server: " + response?.Errors);

            var token = JToken.Parse(response.Data);
            var isArray = typeof(T) != typeof(string) && typeof(T).GetInterface("IEnumerable") != null;
            var output = isArray ? JArray.Parse(token.ToString()).ToObject<T>() : JObject.Parse(token.ToString()).ToObject(typeof(T));
            return output as T;
        }

        #region Login Menu

        private void DoLogin()
        {
            while (true)
            {
                var input = ReadLine("Existing user? Y/N");
                if (input == "Y")
                {
                    var username = ReadLine("Username: ");
                    var password = ReadLine("Password: ");
                    var foundUser =
                        _executer.ExecuteMethod(
                                new Func<string, string, bool, object, User>(DoWorkAndGetServerResponse<User>),
                                "/users/auth", "POST", true, new {Username = username, Password = password}) as
                            User;

                    if (foundUser == null) continue;
                    Console.WriteLine("Logged in as : " + foundUser.FirstName + " " + foundUser.LastName + " ( " +
                                      foundUser.Username + ")");
                    _me = foundUser;
                    break;
                }
                // if N pressed, create user
                CreateObjectProcess<User>("/users/");
            }
        }

        #endregion

        //#region Main Menu Loop
        private void DisplayMenu()
        {
            const string menuString = "\n" +
                                      "1. Profile Data\n" +
                                      "2. View My Lists\n" +
                                      "3. View Movies\n" +
                                      "4. Exit";
            while (true)
            {
                var input = ReadLine(menuString);
                switch (input)
                {
                    case "1":
                        LoadProfileData();
                        ReadKey();
                        break;
                    case "2":
                        ViewMyLists();
                        break;
                    case "3":
                        ViewAllMovies();
                        break;
                    case "4":
                        Environment.Exit(0);
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        ReadKey();
                        break;
                }
            }
        }

        private void LoadProfileData(bool writeToConsole = true)
        {
            _me = GetObjectProcess<User>($"/users/{_me.Id}");
            if(writeToConsole)
                Console.WriteLine(_me?.ToString());//benefit of toString override
        }

        #region List Menu
        private void ViewMyLists()
        {
            const string menuString = "\n1. Create empty list\n2. Back";
            while (true)
            {
                LoadProfileData(false);
                var index = 1;
                var lists = _me == null || _me.Lists.Count == 0
                   ? "No lists."
                   : "Lists: \n" + string.Join("\n", _me.Lists.Select(x => (index++).ToString() + ". " + x.ToString()));
                Console.WriteLine(lists);
                ReadKey();

                var input = ReadLine(menuString);
                switch (input)
                {
                    case "1":
                        //creates the object, assigns the current user, then sends it to server. Need to nullify the recursive refs cause ef is stupid again
                        CreateObjectProcess<List>("/lists/", list =>
                        {
                            _me.Lists = null;
                            _me.Comments = null;
                            _me.IncomingFriendships = null;
                            _me.Ratings = null;
                            _me.OutgoingFriendships = null;
                            list.Users.Add(_me);
                        });
                        ReadKey();
                        break;
                    case "2":
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        ReadKey();
                        break;
                }
            }
        }

        #endregion

        #region Movie Menu

        private void ViewAllMovies()
        {
            const string menuString = "\n\n1. Create movie\n2. Select a movie by index\n3. Back";
            while (true)
            {
                var movies = new List<Movie>();
                var enumerable = _executer.ExecuteMethod(new Func<string, string, bool, object, IEnumerable<Movie>>(DoWorkAndGetServerResponse<IEnumerable<Movie>>), $"/movies/", "GET", false, null) as IEnumerable<Movie>;
                if (enumerable != null)
                    movies.AddRange(enumerable);
                var index = 1;
                var str = movies.Count == 0
                   ? "No movies.\n"
                   : "Movies: \n" + string.Join("\n", movies.Select(x => (index++).ToString() + ". " + x.ToString()));
                Console.WriteLine(str);

                ReadKey();
                var input = ReadLine(menuString);
                switch (input)
                {
                    case "1":
                        CreateObjectProcess<Movie>("/movies/");
                        break;
                    case "2":
                        var selected = _executer.ExecuteMethod(new Func<List<Movie>, Movie>(SelectMovie), movies);
                        if (selected == null)
                            Console.WriteLine("Movie not selected because of error.");
                        else
                        {
                            Console.WriteLine("Selected movie: " + _selectedMovie);
                            DisplayMovieMenu();
                        }
                        break;
                    case "3":
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        break;
                }
            }
        }

        private Movie SelectMovie(IReadOnlyCollection<Movie> movies)
        {
            var input = ReadLine("Insert index (as in the list above):");
            int id;
            if (!int.TryParse(input, out id))
                throw new Exception("Must insert a number");
            if (id < 1 || id > movies.Count)
                throw new Exception("Index must be inside the bounds of the list.");
            _selectedMovie = movies.ElementAt(id - 1);
            return _selectedMovie;
        }

        #endregion

        #region Movie Submenu

        private void DisplayMovieMenu()
        {
            const string menuString = "\n1. Add rating\n2. View ratings\n3. Add comment\n4. View comments\n5. Add to list\n6. Back";
            while (true)
            {
                ReadKey();
                var input = ReadLine(menuString);
                switch (input)
                {
                    case "1":
                        //creates the object, assigns the current Movie, then sends it to server. Need to nullify the recursive refs cause ef is stupid again
                        CreateObjectProcess<Rating>("/ratings/", rating =>
                        {
                            _selectedMovie.Comments = null;
                            _selectedMovie.Lists = null;
                            _selectedMovie.Ratings = null;
                            _selectedMovie.Tags = null;
                            rating.Movie = _selectedMovie;
                            rating.User = _me;
                        });
                        break;
                    case "2":
                        _selectedMovie = GetObjectProcess<Movie>($"/movies/{_selectedMovie.Id}/ratings");
                        var str = _selectedMovie.Ratings.Count == 0
                            ? "No ratings."
                            : string.Join("\n", _selectedMovie.Ratings);
                        Console.WriteLine(str);
                        break;
                    case "3":
                        //creates the object, assigns the current Movie, then sends it to server. Need to nullify the recursive refs cause ef is stupid again
                        CreateObjectProcess<Comment>("/comments/", comment =>
                        {
                            _selectedMovie.Comments = null;
                            _selectedMovie.Lists = null;
                            _selectedMovie.Ratings = null;
                            _selectedMovie.Tags = null;
                            comment.Movie = _selectedMovie;
                            comment.User = _me;
                        });
                        break;
                    case "4":
                        _selectedMovie = GetObjectProcess<Movie>($"/movies/{_selectedMovie.Id}/comments");
                        str = _selectedMovie.Comments.Count == 0
                            ? "No comments."
                            : string.Join("\n------------------------\n", _selectedMovie.Comments);
                        Console.WriteLine(str);
                        break;
                    case "5":
                        var addedIntoList = _executer.ExecuteMethod(new Func<bool, List>(AddMovieToListRoutine), true);
                        if (addedIntoList == null)
                            Console.WriteLine("Movie not added to list because of error.");
                        else
                        {
                            Console.WriteLine($"Movie {_selectedMovie} added to list {addedIntoList}.");
                            DisplayMovieMenu();
                        }
                        break;
                    case "6":
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        break;
                }
            }
        }

        private List AddMovieToListRoutine(bool intro)
        {
            LoadProfileData(false);
            if (_me.Lists.Count == 0)
                throw new Exception("You have no lists. Go to the list menu and create one.");

            const string menuString = "\nInsert index of the list to Add Movie To (as in the list above):";
            var index = 1;
            var str = "Lists: \n" + string.Join("\n", _me.Lists.Select(x => (index++).ToString() + ". " + x.ToString())) + menuString;
            var input = ReadLine(str);

            int id;
            if (!int.TryParse(input, out id))
                throw new Exception("Must insert a number");
            if (id < 1 || id > _me.Lists.Count)
                throw new Exception("Index must be inside the bounds of the list.");

            var selectedList = _me.Lists.ElementAt(id - 1);

            selectedList.Movies.Clear();
            selectedList.Movies.Add(_selectedMovie);
            DoWorkAndGetServerResponse<List>($"/lists/{selectedList.Id}", "PUT", true, selectedList);

            return selectedList;
        }
        #endregion
    }
}
