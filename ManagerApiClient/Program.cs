﻿using Utilities;
using Utilities.Logger;

namespace ManagerApiClient
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var executer = new Executer(new ConsoleLogger());
            var manager = new ConsoleManager(executer);
            manager.Execute();
        }
    }
}
