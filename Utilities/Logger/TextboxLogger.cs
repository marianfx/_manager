﻿using System;
using System.Windows.Forms;

namespace Utilities.Logger
{
    public class TextboxLogger: ILogger
    {
        private readonly TextBox _textBox;

        public TextboxLogger(TextBox textBox)
        {
            _textBox = textBox;
        }
        public void Log(string message)
        {
            _textBox.AppendText("\n\r" + message + "\n\r");
        }
    }
}
