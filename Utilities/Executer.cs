﻿using System;
using Utilities.Logger;

namespace Utilities
{
    public class Executer
    {
        private readonly ILogger _logger;

        public Executer(ILogger logger)
        {
            _logger = logger;
        }

        public object ExecuteMethod(Delegate method, params object[] args)
        {
            var methodName = method.Method.Name;
            _logger.Log($"Starting executing '{methodName}'.");
            object result = null;
            try
            {

                result = method.DynamicInvoke(args);
                _logger.Log($"Success executing '{methodName}'." /*\n\r\tOutput: {(result ?? "None")}"*/);
            }
            catch (Exception e)
            {
                result = null;
                _logger.Log($"Error executing '{methodName}'.Details:\n\r\t{Statics.GetDetailedError(e)}.");
            }

            return result;
        }

        public ILogger GetLogger()
        {
            return _logger;
        }
    }
}
