﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Utilities
{
    public class Statics
    {
        public static Guid ParseGuidWithWebEx(string id)
        {
            Guid guid;
            Guid.TryParse(id, out guid);
            if (guid == null || guid == Guid.Empty)
                throw new Exception("Id cannnot be null.");
            return guid;
        }

        public static string GetDetailedError(Exception e)
        {
            if (e == null)
                return "";
            var originalMsg = e.Message;
            var detailMsg = "";
            while (e.InnerException != null)
            {
                detailMsg = e.InnerException.Message;
                e = e.InnerException;
            }
            return originalMsg + " Inner: " + detailMsg;
        }

        public static object GetDefault(Type t)
        {
            return typeof(Statics).GetMethod("GetDefaultGeneric").MakeGenericMethod(t).Invoke(null, null);
        }

        public static T GetDefaultGeneric<T>()
        {
            return default(T);
        }

        /// <summary>
        /// The POST / PUT body of the request is found in the 'data' Stream. The server lets inside the server JSON data only, so we try parsing it and make it into an object.
        /// </summary>
        /// <typeparam name="T">The type of the object we want to output.</typeparam>
        /// <param name="data">The stream contained in the request body.</param>
        /// <param name="obj">The object constructed / updated with data from request body.</param>
        /// <param name="checkForRequiredParams">Set this to true when creating the T object so the method checks the body of the request has at least the required parameters for this object to construct (on update, will check if any fields match, if no - ignore them all)</param>
        /// <returns></returns>
        public static T GetJsonObjectFromStream<T>(Stream data, ref T obj, bool checkForRequiredParams = true)
            where T : class, new()
        {
            using (var reader = new StreamReader(data))
            {
                var jString = reader.ReadToEnd();
                return TransformJsonToObject(jString, ref obj, checkForRequiredParams);
            }
        }

        public static T TransformJsonToObject<T>(string jString, ref T obj, bool checkForRequiredParams)
            where T: class, new()
        {
            JObject json;
            var type = typeof(T);
            if(obj == null)
                obj = new T();

            try
            {
                var token = JToken.Parse(jString);
                json = JObject.Parse(token.ToString());
                obj = json.ToObject(typeof(T)) as T;
            }
            catch(Exception e)
            {
                throw new Exception("Cannot parse your input data as JSON.");
            }

            foreach (var prop in type.GetProperties())
            {
                var name = prop.Name;

                if (checkForRequiredParams && json[name] == null && prop.GetValue(obj) == null)// if json value it's null, make sure we are not null/defaultlyinited
                    throw new Exception("The body must contain the property " + name + ".");

                if (json[name] == null) continue;
                //ignore lists
                if(prop.PropertyType != typeof(string) && prop.PropertyType.GetInterface("IEnumerable") != null) continue;
                
            }
            return obj;
        }
    }
}
