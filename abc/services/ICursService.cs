﻿using services.DataContracts;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ICursService
    {
        [OperationContract]
        Curs Get(Guid id);


        [OperationContract]
        IEnumerable<Curs> GetAll();

        [OperationContract]
        void Create(Curs composite);
    }
}
