﻿using System;
using System.ServiceModel;

namespace services
{
    public class Program
    {
        public static void Main(params string[] args)
        {
            // create curs service
            var baseAddress = new Uri("http://127.0.0.1:2200/curs");
            var binding = new BasicHttpBinding();
            var cService = new ServiceHost(typeof(CursService), baseAddress);
            cService.AddServiceEndpoint(typeof(ICursService), binding, baseAddress);
            cService.Open();
            Console.WriteLine("Curs service started.");

            // the same manner, add the next ones here

            Console.WriteLine("All services up and running.");
            Console.ReadKey();
        }
    }
}
