﻿using System;
using System.Runtime.Serialization;

namespace services.DataContracts
{
    [DataContract]
    public class Curs
    {
        [DataMember]
        public string titlu_curs { set; get; }
        
        [DataMember]
        public int an { set; get; }
        
        [DataMember]
        public int semestru { set; get; }

        public Curs() { } // need to have this

        public Curs(database.Models.Curs curs)
        {
            titlu_curs = curs.titlu_curs;
            an = curs.an;
            semestru = curs.semestru;
        }

        public database.Models.Curs GetCurs()
        {
            return new database.Models.Curs
            {
                Id = Guid.NewGuid(),
                titlu_curs = titlu_curs,
                an = an,
                semestru = semestru
            };
        }

        public override string ToString()
        {
            return "Curs " + titlu_curs + ", an " + an + ", semestru " + semestru + "\n";
        }
    }
}
