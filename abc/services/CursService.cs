﻿using services.DataContracts;
using System;
using database.Repositories;
using System.ServiceModel;
using System.Collections.Generic;

namespace services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class CursService : ICursService
    {
        private CursRepository _repository;

        CursService()
        {
            _repository = new CursRepository();
        }

        public void Create(Curs curs)
        {
            _repository.Create(curs.GetCurs());
        }

        public Curs Get(Guid id)
        {
            return new Curs (_repository.GetById(id));
        }

        public IEnumerable<Curs> GetAll()
        {
            List<Curs> cursuri = new List<Curs>();
            var cursuriDb = _repository.GetAll();
            foreach (var curs in cursuriDb)
            {
                cursuri.Add(new Curs(curs));
            }
            return cursuri;
        }
    }
}
