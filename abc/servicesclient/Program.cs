﻿
using System;

namespace servicesclient
{
    class Program
    {
        private static string ReadLine(string display)
        {
            Console.Write($"\n{display} ");
            return Console.ReadLine();
        }

        static void Main(string[] args)
        {
            var ctrl = new Controller();
            ctrl.StartServices();

            bool doIt = false;
            string answer = String.Empty;


            // run stuff for curs, asking for each operation
            answer = ReadLine("Do you want to create a course?");
            doIt = answer == "Y" ? true : false;
            if(doIt)
                ctrl.CreateCurs();

            answer = ReadLine("Do you want to view all the courses?");
            doIt = answer == "Y" ? true : false;
            if (doIt)
                ctrl.GetAlLCourses();

            Console.ReadKey();
        }
    }
}
