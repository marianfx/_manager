﻿using services;
using services.DataContracts;
using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace servicesclient
{
    public class Controller
    {
        private ICursService cursService;

        public void StartServices()
        {
            // create curs service
            var cursUri = new Uri("http://127.0.0.1:2200/curs");
            var cursEndpoint = new EndpointAddress(cursUri);
            var binding = new BasicHttpBinding();
            var cursFactory = new ChannelFactory<ICursService>(binding, cursEndpoint);
            cursService = cursFactory.CreateChannel();

            // create next services here
        }

        public void CreateCurs()
        {
            var curs = CreateObjectFromInput<Curs>();// this is from services.DataContracts, not from database.models
            cursService.Create(curs);
        }

        public void GetAlLCourses()
        {
            Console.WriteLine("\nCourses:\n");
            var courses = cursService.GetAll();
            var list = new List<Curs>(courses);
            if (list.Count == 0)
                Console.WriteLine("No data.\n");

            foreach (var curs in courses)
            {
                Console.Write(curs);// functioneaza fiindca are ToString
            }
        }

        #region Generic Model Creation

        private static string ReadLine(string display)
        {
            Console.Write($"\n{display} ");
            return Console.ReadLine();
        }
        
        /// <summary>
        /// Reads all the required properties of an object from the console, builds the object and returns it.
        /// </summary>
        /// <typeparam name="T">Represents a class (any, usually derived from ModelBase or an IEnumerable), from which objects can be created (hence the new() constraint)</typeparam>
        /// <returns>T object</returns>
        public T CreateObjectFromInput<T>()
            where T : class, new()
        {
            Console.WriteLine("Insert data to create an " + typeof(T).Name + ": ");
            var created = new T();

            foreach (var prop in typeof(T).GetProperties())
            {
                var input = ReadLine(prop.Name + ": ");
                prop.SetValue(created, Convert.ChangeType(input, prop.PropertyType), null);
            }
            return created;
        }

        #endregion
    }
}
