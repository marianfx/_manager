﻿using System;
using System.Collections.Generic;
using System.Linq;
using database.Models;
using database.Repositories;
using database;

namespace abc
{
    public class Program
    {

        static void Main(string[] args)
        {
            DbContextProvider dbProvider = new DbContextProvider();

            var student1 = new Student
            {
                nume = "Coseru",
                prenume = "Xena",
                an = 3,
                grupa = "B2"
            };

            var student2 = new Student
            {
                nume = "Brunchi",
                prenume = "Bogdan",
                an = 2,
                grupa = "B2"
            };

            var student3 = new Student
            {
                nume = "Coseru",
                prenume = "Doris",
                an = 1,
                grupa = "B2"
            };


            //var database = dbProvider.GetDbContext();
            var studentRepository = new StudentRepository();
            var nr_matricol = studentRepository.Insert(student1);
            /*studentRepository.Insert(student2);
            studentRepository.Insert(student3);
            Console.WriteLine(studentRepository.GetAll().Stringify());
            Console.WriteLine();
            var updateStudent = studentRepository.GetByNr_matricol(nr_matricol);
            updateStudent.nume = "Coseruuuuuu";
            studentRepository.Update(updateStudent);
            Console.WriteLine(studentRepository.GetAll().Stringify());
            Console.WriteLine();
            studentRepository.Remove(updateStudent.nr_matricol);
            Console.WriteLine(studentRepository.GetAll().Stringify());
            studentRepository.GetAll().ForEach(x => studentRepository.Remove(x.nr_matricol));*/

        }

        /*private string Stringify(this IEnumerable<Student> studenti)
        {
            return string.Join("\n", studenti.Select(x => x.nume + " " + x.prenume + " - " + x.nr_matricol));
        }*/
    }
}
