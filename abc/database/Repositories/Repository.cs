﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using database.Models;

namespace database.Repositories
{
    public class Repository<T>: IRepository<T>
        where T: IModel
    {
        private DbContextProvider dbProvider = new DbContextProvider();

        public T Create(T entity)
        {
            var db = dbProvider.GetDbContext();

            db.Set<T>().Add(entity);
            db.SaveChanges();
            return entity;
        }

        public T GetById(Guid id)
        {
            var db = dbProvider.GetDbContext();
            var output = db.Set<T>().FirstOrDefault(e => e.Id == id);
            return output;
        }

        public IEnumerable<T> GetAll()
        {
            var db = dbProvider.GetDbContext();
            var entities = db.Set<T>().ToList();
            return entities;
        }

        public T Update(T entity)
        {
            var db = dbProvider.GetDbContext();
            db.Set<T>().AddOrUpdate(entity);
            db.SaveChanges();
            return entity;
        }

        public T Remove(Guid id)
        {
            var db = dbProvider.GetDbContext();
            var entity = GetById(id);
            db.Set<T>().Remove(entity);
            db.SaveChanges();
            return entity;
        }
    }
}
