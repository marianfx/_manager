﻿using System;
using System.Collections.Generic;
using database.Models;

namespace database.Repositories
{
    public interface IRepository<T> where T : IModel
    {
        T Create(T entity);
        T Update(T entity);
        IEnumerable<T> GetAll();
        T GetById(Guid id);
        T Remove(Guid id);
    }
}
