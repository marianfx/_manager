﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace database.Models
{
    public class Curs : IModel
    {
        [Required]
        public string titlu_curs { set; get; }

        [Required]
        public int an { set; get; }

        [Required]
        public int semestru { set; get; }

        public Curs()
        {
        }
    }
}
