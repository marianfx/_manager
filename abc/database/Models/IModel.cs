﻿using System;
using System.ComponentModel.DataAnnotations;

namespace database.Models
{
    public abstract class IModel
    {
        [Key]
        [Required]
        public Guid Id { get; set; }
    }
}
