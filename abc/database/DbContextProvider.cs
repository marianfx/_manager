﻿namespace database
{
    public class DbContextProvider
    {
        private MyDataBase dbContext;
        public MyDataBase GetDbContext()
        {
            return dbContext ?? (dbContext = new MyDataBase());
        }
    }
}
