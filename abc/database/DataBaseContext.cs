﻿using System.Data.Entity;
using database.Models;

namespace database
{
    public class MyDataBase:DbContext
    {
        public DbSet<Curs> Cursuri { get; set; }

        public MyDataBase(): base()
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // example of how to map many to many from here
            //modelBuilder.Entity<Curs>()
            //    .HasMany(p => p.ListProfi)
            //    .WithMany(c => c.CursuriPredate)
            //    .Map(up =>
            //    {
            //        up.MapLeftKey("ID_prof");
            //        up.MapRightKey("ID_curs");
            //        up.ToTable("ProfiCurs");
            //    });
        }


    }
}
