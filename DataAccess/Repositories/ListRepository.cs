﻿using DataAccess.Models;
using DataAccess.Repositories.Interfaces;

namespace DataAccess.Repositories
{
    public class ListRepository : Repository<List>, IListRepository
    {
        public ListRepository(IManagerContext platformManagement) : base(platformManagement)
        {
        }

        public void LoadAllDataForList(List list)
        {
            if (!Context.Entry(list).Collection(u => u.Users).IsLoaded)
                Context.Entry(list).Collection(u => u.Users).Load();

            if (!Context.Entry(list).Collection(u => u.Movies).IsLoaded)
                Context.Entry(list).Collection(u => u.Movies).Load();
            
        }
    }
}
