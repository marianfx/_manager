﻿using DataAccess.Repositories.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DataAccess.Models.Base;

namespace DataAccess.Repositories
{
    public abstract class Repository<T> : IRepository<T>
    where T : ModelBase
    {
        protected readonly IManagerContext Context;

        protected Repository(IManagerContext context)
        {
            Context = context;
        }

        public T Create(T p)
        {
            Context.Set<T>().Add(p);
            //make this biutiful ef thing create only the object, not the inner objects all over again (just make the associations)
            foreach (var property in p.GetType().GetProperties())
            {
                if (property.GetValue(p) == null || property.PropertyType == typeof(string)) // add string because it s the same as IEnumerable
                    continue;

                if(property.PropertyType.BaseType == typeof(ModelBase))// here mark as unchanged the models
                    Context.Entry(property.GetValue(p)).State = EntityState.Unchanged;

                if (property.PropertyType.GetInterface("IEnumerable") == null) continue;
                foreach (var item in (IEnumerable) property.GetValue(p, null))
                {
                    Context.Entry(item).State = EntityState.Unchanged;
                }
            }
            Context.SaveChanges();
            return p;
        }

        public void Update(T p)
        {
            var existing = Context.Set<T>().FirstOrDefault(x => x.Id == p.Id);
            foreach (var prop in typeof(T).GetProperties())
            {
                prop.SetValue(existing, prop.GetValue(p));

                if (prop.GetValue(existing) == null || prop.PropertyType == typeof(string)) // add string because it s the same as IEnumerable
                    continue;

                if (prop.PropertyType.BaseType == typeof(ModelBase))// here mark as unchanged the models
                    Context.Entry(prop.GetValue(existing)).State = EntityState.Unchanged;

                if (prop.PropertyType.GetInterface("IEnumerable") == null) continue;
                foreach (var item in (IEnumerable)prop.GetValue(existing, null))
                {
                    Context.Entry(item).State = EntityState.Unchanged;
                }
            }
            Context.Entry(existing).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public void Delete(T p)
        {
            Context.Set<T>().Remove(p);
            Context.SaveChanges();
        }

        public T GetById(Guid id)
        {
            return Context.Set<T>().FirstOrDefault(p => p.Id == id);
        }

        public IEnumerable<T> GetAll()
        {
            return Context.Set<T>();
        }
    }
}
