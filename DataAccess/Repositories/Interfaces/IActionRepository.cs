﻿using DataAccess.Models;

namespace DataAccess.Repositories.Interfaces
{
    public interface IActionRepository: IRepository<Action>
    {
    }
}
