﻿using DataAccess.Models;

namespace DataAccess.Repositories.Interfaces
{
    public interface IMovieRepository: IRepository<Movie>
    {
        void LoadRatingsForMovie(Movie movie);
        void LoadCommentsForMovie(Movie movie);
    }
}
