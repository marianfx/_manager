﻿using DataAccess.Models;

namespace DataAccess.Repositories.Interfaces
{
    public interface IUserRepository: IRepository<User>
    {
        User GetByUsernameAndPass(string username, string password);
        User GetByUsername(string username);
        void LoadAllDataForUser(User user);
    }
}
