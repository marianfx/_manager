﻿using DataAccess.Models;

namespace DataAccess.Repositories.Interfaces
{
    public interface IListRepository: IRepository<List>
    {
    }
}
