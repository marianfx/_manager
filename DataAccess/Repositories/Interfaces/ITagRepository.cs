﻿using DataAccess.Models;

namespace DataAccess.Repositories.Interfaces
{
    public interface ITagRepository: IRepository<Tag>
    {
    }
}
