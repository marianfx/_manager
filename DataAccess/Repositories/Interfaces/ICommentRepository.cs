﻿using DataAccess.Models;

namespace DataAccess.Repositories.Interfaces
{
    public interface ICommentRepository: IRepository<Comment>
    {
    }
}
