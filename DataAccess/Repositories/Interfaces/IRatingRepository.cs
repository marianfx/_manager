﻿using DataAccess.Models;

namespace DataAccess.Repositories.Interfaces
{
    public interface IRatingRepository: IRepository<Rating>
    {
    }
}
