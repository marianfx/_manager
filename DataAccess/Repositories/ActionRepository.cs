﻿using DataAccess.Models;
using DataAccess.Repositories.Interfaces;

namespace DataAccess.Repositories
{
    public class ActionRepository : Repository<Action>, IActionRepository
    {
        public ActionRepository(IManagerContext platformManagement) : base(platformManagement)
        {
        }
    }
}
