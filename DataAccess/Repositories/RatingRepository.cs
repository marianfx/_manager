﻿using System;
using System.Linq;
using DataAccess.Models;
using DataAccess.Repositories.Interfaces;

namespace DataAccess.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(IManagerContext platformManagement) : base(platformManagement)
        {
        }

        public Rating GetById(Guid id)
        {
            var r = Context.Set<Rating>().FirstOrDefault(p => p.Id == id);

            if (!Context.Entry(r).Reference(u => u.User).IsLoaded)
                Context.Entry(r).Reference(u => u.User).Load();
            return r;
        }
    }
}
