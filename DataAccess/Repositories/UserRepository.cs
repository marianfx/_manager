﻿using System.Linq;
using DataAccess.Models;
using DataAccess.Repositories.Interfaces;

namespace DataAccess.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IManagerContext platformManagement) : base(platformManagement)
        {
        }

        public User GetByUsernameAndPass(string username, string password)
        {
            var user = (from u in Context.Users
                where u.Username.Equals(username) && u.Password.Equals(password)
                select u).FirstOrDefault();
            return user;
        }

        public User GetByUsername(string username)
        {
            var user = (from u in Context.Users
                        where u.Username.Equals(username)
                        select u).FirstOrDefault();
            return user;
        }

        public void LoadAllDataForUser(User user)
        {
            if (!Context.Entry(user).Collection(u => u.Lists).IsLoaded)
                Context.Entry(user).Collection(u => u.Lists).Load();

            if (!Context.Entry(user).Collection(u => u.Ratings).IsLoaded)
                Context.Entry(user).Collection(u => u.Ratings).Load();

            if (!Context.Entry(user).Collection(u => u.Comments).IsLoaded)
                Context.Entry(user).Collection(u => u.Comments).Load();

            foreach (var list in user.Lists)
            {
                if (!Context.Entry(list).Collection(l => l.Movies).IsLoaded)
                    Context.Entry(list).Collection(l => l.Movies).Load();
            }
        }
    }
}
