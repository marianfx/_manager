﻿using System;
using System.Linq;
using DataAccess.Models;
using DataAccess.Repositories.Interfaces;

namespace DataAccess.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(IManagerContext platformManagement) : base(platformManagement)
        {
        }

        public Comment GetById(Guid id)
        {
            var r = Context.Set<Comment>().FirstOrDefault(p => p.Id == id);

            if (!Context.Entry(r).Reference(u => u.User).IsLoaded)
                Context.Entry(r).Reference(u => u.User).Load();

            return r;
        }
    }
}
