﻿using DataAccess.Models;
using DataAccess.Repositories.Interfaces;

namespace DataAccess.Repositories
{
    public class TagRepository : Repository<Tag>, ITagRepository
    {
        public TagRepository(IManagerContext platformManagement) : base(platformManagement)
        {
        }
    }
}
