﻿using System.Linq;
using DataAccess.Models;
using DataAccess.Repositories.Interfaces;

namespace DataAccess.Repositories
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(IManagerContext platformManagement) : base(platformManagement)
        {
        }

        public void LoadRatingsForMovie(Movie movie)
        {
            if(!Context.Entry(movie).Collection(m => m.Ratings).IsLoaded)
                Context.Entry(movie).Collection(m => m.Ratings).Load();
            movie.Ratings.ToList().ForEach(r =>
                {
                    if (!Context.Entry(r).Reference(rr => rr.User).IsLoaded)
                        Context.Entry(r).Reference(rr => rr.User).Load();
                }
            );
        }

        public void LoadCommentsForMovie(Movie movie)
        {
            if (!Context.Entry(movie).Collection(m => m.Comments).IsLoaded)
                Context.Entry(movie).Collection(m => m.Comments).Load();
            movie.Comments.ToList().ForEach(c =>
            {
                if (!Context.Entry(c).Reference(cc => cc.User).IsLoaded)
                    Context.Entry(c).Reference(cc => cc.User).Load();
            }
            );
        }
    }
}
