﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.Models.Base;

namespace DataAccess.Models
{
    public class Movie: ModelBase
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(2048)]
        public string Description { get; set; } = "No detailed description.";

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ReleaseDate { get; set; }

        [Required]
        [StringLength(500)]
        [DataType(DataType.Url)]
        public string PhotoUrl { get; set; } = "defaultPoster.png";

        [Required]
        [DataType(DataType.Url)]
        public string Trailer { get; set; } = "https://www.youtube.com/watch?v=kfoJUeyMsOE";

        [Required]
        public string ImdbId { get; set; }

        [Required]
        public decimal RatingAvgCritics { get; set; } = 0.0M;//precision defined in context

        [Required]
        public decimal RatingAvgUsers { get; set; } = 0.0M; //precision defined in context

        public virtual ICollection<Tag> Tags { get; set; } = new List<Tag>();

        public virtual ICollection<List> Lists { get; set; } = new List<List>();// * - *

        [InverseProperty("Movie")]
        public virtual ICollection<Rating> Ratings { get; set; } = new List<Rating>();

        [InverseProperty("Movie")]
        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public override string ToString()
        {
            return Name + " (" + ReleaseDate.Year + ")";
        }
    }
}
