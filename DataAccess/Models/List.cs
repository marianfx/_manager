﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DataAccess.Models.Base;
using DataAccess.Models.Enums;

namespace DataAccess.Models
{
    public class List: ModelBase
    {
        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(2048)]
        public string Description { get; set; } = "No detailed description.";

        [Required]
        public Visibility Visibility { get; set; } = Visibility.Private;
        
        [Required]
        public decimal RatingAvgUsers { get; set; } = 0.0M; //precision defined in context

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Timestamp { get; set; } = DateTime.Now;

        public virtual ICollection<Movie> Movies { get; set; } = new List<Movie>(); // * - * 

        public virtual ICollection<User> Users { get; set; } = new List<User>(); // * - *

        [InverseProperty("List")]
        public virtual ICollection<Rating> Ratings { get; set; } = new List<Rating>();

        [InverseProperty("List")]
        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public override string ToString()
        {
            var data = Name +  " (" + Timestamp.ToShortDateString() + ") - " + Movies.Count + " movies.\n";
            if (Movies.Count == 0)
                data += "\t\tNo movies.";
            return Movies.Aggregate(data, (current, movie) => current + ("\n\t\t" + movie.ToString()));
        }
    }
}
