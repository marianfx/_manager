﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using DataAccess.Models.Base;

namespace DataAccess.Models
{
    public class User: ModelBase
    {
        [Required]
        [Index(IsUnique = true)]
        [StringLength(20, MinimumLength = 2)]
        public string Username { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(100)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [StringLength(2014, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(500)]
        [DataType(DataType.Url)]
        public string PhotoUrl { get; set; } = "defaultImage.png";

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime RegistrationDate { get; set; } = DateTime.Now;

        [InverseProperty("User")]
        public virtual ICollection<Friendship> OutgoingFriendships { get; set; } = new List<Friendship>();

        [InverseProperty("Friend")]
        public virtual ICollection<Friendship> IncomingFriendships { get; set; } = new List<Friendship>();

        public virtual ICollection<List> Lists { get; set; } = new List<List>();

        [InverseProperty("User")]
        public virtual ICollection<Rating> Ratings { get; set; } = new List<Rating>();

        [InverseProperty("User")]
        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();

        public override string ToString()
        {
            var data = FirstName + " " + LastName + " (" + Username + ") - " + Email + "\n";
            data += "Lists:\n";
            if (Lists.Count == 0)
                data += "\tNo lists.";
            return Lists.Aggregate(data, (current, list) => current + ("\n\t" + list.ToString()));
        }
    }
}
