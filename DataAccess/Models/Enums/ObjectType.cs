﻿namespace DataAccess.Models.Enums
{
    public enum ObjectType
    {
        User,
        Movie,
        List,
        Rating,
        Tag,
        Comment
    }
}
