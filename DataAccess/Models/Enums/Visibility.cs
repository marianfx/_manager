﻿namespace DataAccess.Models.Enums
{
    public enum Visibility
    {
        Private,
        Unlisted,
        Public
    }
}
