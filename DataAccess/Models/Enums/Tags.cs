﻿namespace DataAccess.Models.Enums
{
    public enum Tags
    {
        Star,
        Genre,
        Director
    }
}
