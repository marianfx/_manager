﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.Models.Base;

namespace DataAccess.Models
{
    public class Friendship: ModelBase
    {
        [Required, Key]
        [Column(Order = 1)]
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required, Key]
        [Column(Order = 2)]
        public Guid FriendId { get; set; }
        [ForeignKey("FriendId")]
        public virtual User Friend { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Timestamp { get; set; } = DateTime.Now;
    }
}
