﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.Models.Base;
using DataAccess.Models.Enums;

namespace DataAccess.Models
{
    public class Action: ModelBase
    {

        [Required, Key]
        [Column(Order = 1)]
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }

        [Required, Key]
        [Column(Order = 2)]
        public Guid ObjectId { get; set; }

        [Required]
        public ObjectType ObjectType { get; set; } = ObjectType.Comment;

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [StringLength(2048)]
        public string Value { get; set; }

        [StringLength(500)]
        [DataType(DataType.Url)]
        public string PhotoUrl { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Timestamp { get; set; } = DateTime.Now;
    }
}
