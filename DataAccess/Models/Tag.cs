﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.Models.Base;

namespace DataAccess.Models
{
    public class Tag: ModelBase
    {

        [Required]
        [Index(IsUnique = true)]
        [StringLength(200)]
        public string Name { get; set; }

        [StringLength(2048)]
        public string Description { get; set; } = "No detailed description.";

        [Required]
        public Enums.Tags TagName { get; set; } = Enums.Tags.Star;

        public virtual ICollection<Movie> Movies { get; set; } = new List<Movie>();
    }
}
