﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccess.Models.Base;
using DataAccess.Models.Enums;

namespace DataAccess.Models
{
    public class Comment: ModelBase
    {
        [Required]
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
        
        public Guid? MovieId { get; set; }
        [ForeignKey("MovieId")]
        public virtual Movie Movie { get; set; }
        
        public Guid? ListId { get; set; }
        [ForeignKey("ListId")]
        public virtual List List { get; set; }

        [Required]
        public ObjectType ObjectType { get; set; } = ObjectType.Movie;

        [Required]
        [StringLength(2048)]
        public string Value { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime Timestamp { get; set; } = DateTime.Now;

        public override string ToString()
        {
            return User.Username + " (" + Timestamp.ToShortDateString() + ")\n" + Value + "\n----------------------------\n";
        }
    }
}
