﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccess.Models.Base
{
    public class ModelBase
    {
        [Required]
        [Key]
        [Column(Order = 0)]
        public Guid Id { get; set; }
    }
}
