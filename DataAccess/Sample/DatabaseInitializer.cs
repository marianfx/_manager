﻿using System;
using System.Collections.Generic;
using DataAccess.Models;
using DataAccess.Models.Enums;
using DataAccess.Repositories.Interfaces;
using Utilities;

namespace DataAccess.Sample
{
    public class DatabaseInitializer
    {
        private readonly IManagerContext _context;
        private Guid _userGuid = Guid.Parse("56b78c4d-9d3c-4b52-be24-93c12dec0f60");
        private readonly Executer _executer;
        private readonly IUserRepository _userRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IListRepository _listRepository;
        private readonly ICommentRepository _commentRepository;

        #region "Fields"
        User mfx = new User
        {
            Id = Guid.NewGuid(),
            Username = "mfx",
            FirstName = "Marian",
            LastName = "Fx",
            Email = "mfx@email.com",
            Password = "parolamea"
        };

        Movie movie1 = new Movie
        {
            Id = Guid.NewGuid(),
            Name = "Arrival",
            ImdbId = "tt2543164",
            Description = "When twelve mysterious spacecraft appear around the world, linguistics professor Louise Banks is tasked with interpreting the language of the apparent alien visitors.",
            ReleaseDate = new DateTime(2016, 11, 11),
            RatingAvgCritics = 8.1M,
            RatingAvgUsers = 8.0M
        };

        Movie movie2 = new Movie
        {
            Id = Guid.NewGuid(),
            Name = "Logan",
            ImdbId = "tt3315342",
            Description = "In the near future, a weary Logan cares for an ailing Professor X in a hide out on the Mexican border. But Logan\'s attempts to hide from the world and his legacy are up-ended when a young mutant arrives, being pursued by dark forces.",
            ReleaseDate = new DateTime(2017, 3, 3),
            RatingAvgCritics = 7.7M,
            RatingAvgUsers = 8.7M
        };

        List list = new List
        {
            Id = Guid.NewGuid(),
            Name = "Watchlist",
            Description = "Contains all the movies I want to watch.",
            RatingAvgUsers = 9.0M,
            Visibility = Visibility.Private
        };

        private Comment loganComment = new Comment
        {
            Id = Guid.NewGuid(),
            Value = "Meh, did not really like it",
            ObjectType = ObjectType.Movie
    };
        #endregion

        public DatabaseInitializer(IManagerContext context, IUserRepository userRepository, IMovieRepository movieRepository, IListRepository listRepository, ICommentRepository commentRepository, Executer executer)
        {
            _context = context;
            _executer = executer;
            _userRepository = userRepository;
            _movieRepository = movieRepository;
            _listRepository = listRepository;
            _commentRepository = commentRepository;
        }

        public void ExecuteAll()
        {
            _executer.ExecuteMethod(new System.Action(AddAnUser));
            _executer.ExecuteMethod(new System.Action(AddTwoMovies));
            _executer.ExecuteMethod(new System.Action(AddTheMoviesToAList));
            _executer.ExecuteMethod(new System.Action(AddTheCommentToFirstMovie));
        }

        public void AddAnUser()
        {
            mfx.Id = _userGuid;
            _userRepository.Create(mfx);
        }

        public void AddTwoMovies()
        {
            _movieRepository.Create(movie1);
            _movieRepository.Create(movie2);
        }

        public void AddTheMoviesToAList()
        {
            list.Users = new List<User>() {mfx};
            _listRepository.Create(list);
            list.Movies = new List<Movie>() {movie1, movie2};
            _listRepository.Update(list);
            
        }

        public void AddTheCommentToFirstMovie()
        {
            loganComment.UserId = mfx.Id;
            loganComment.MovieId = movie2.Id;
            _commentRepository.Create(loganComment);
        }
    }
}
