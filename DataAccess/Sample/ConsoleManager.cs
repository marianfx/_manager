﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataAccess.Models;
using DataAccess.Repositories.Interfaces;
using Utilities;
using static System.String;

namespace DataAccess.Sample
{
    public class ConsoleManager
    {
        private readonly Executer _executer;
        private readonly IManagerContext _context;
        private readonly IUserRepository _userRepository;
        private readonly IMovieRepository _movieRepository;
        private readonly IListRepository _listRepository;
        private readonly ICommentRepository _commentRepository;
        private readonly IRatingRepository _ratingRepository;
        private User _me;
        private Movie _selectedMovie;

        public ConsoleManager(IManagerContext context, IUserRepository userRepository, IMovieRepository movieRepository,
            IListRepository listRepository, ICommentRepository commentRepository, IRatingRepository ratingRepository, Executer executer)
        {
            _executer = executer;
            _context = context;
            _movieRepository = movieRepository;
            _listRepository = listRepository;
            _commentRepository = commentRepository;
            _userRepository = userRepository;
            _ratingRepository = ratingRepository;
        }

        private string ReadLine(string display)
        {
            Console.Write($"\n{display} ");
            return Console.ReadLine();
        }

       void ReadKey()
        {
            Console.Write("Press any key to continue...");
            Console.ReadKey();
        }

        public void Execute()
        {
            DoLogin();
            DisplayMenu();
        }

        #region Login Menu

        void DoLogin()
        {
            while (true)
            {
                var input = ReadLine("Existing user? Y/N");
                if (input == "Y")
                {
                    var username = ReadLine("Username: ");
                    var password = ReadLine("Password: ");
                    var foundUser = _userRepository.GetByUsernameAndPass(username, password);
                    if (foundUser == null)
                        Console.WriteLine("Invalid user. Try again.");
                    else
                    {
                        Console.WriteLine("Logged in as : " + foundUser.FirstName + " " + foundUser.LastName + " ( " +
                                          foundUser.Username + ")");
                        _me = foundUser;
                        break;
                    }
                }
                else
                {
                    var created = _executer.ExecuteMethod(new Func<bool, User>(CreateUserFromInput), true);
                    if(created == null)
                        Console.WriteLine("Cannot create user. Please try again.");
                }
            }
        }

        public User CreateUserFromInput(bool x)
        {
            Console.WriteLine("Insert data to create an user: ");
            var created = new User {Id = Guid.NewGuid()};
            foreach (var prop in typeof(User).GetProperties())
            {
                if (prop.GetValue(created) != null) continue;// skip default value props
                Console.Write(prop.Name + ": ");
                var input = Console.ReadLine();
                prop.SetValue(created, Convert.ChangeType(input, prop.PropertyType), null);
            }
            _userRepository.Create(created);
            return created;
        }
        #endregion

#region Main Menu Loop
        void DisplayMenu()
        {
            const string menuString = "\n" +
                                      "1. Profile Data\n" +
                                      "2. View My Lists\n" +
                                      "3. View Movies\n" +
                                      "4. Exit";
            while (true)
            {
                var input = ReadLine(menuString);
                switch (input)
                {
                    case "1":
                        ViewProfileData();
                        ReadKey();
                        break;
                    case "2":
                        ViewMyLists();
                        break;
                    case "3":
                        ViewAllMovies();
                        break;
                    case "4":
                        Environment.Exit(0);
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        ReadKey();
                        break;
                }
            }
        }

        void ViewProfileData()
        {
            _userRepository.LoadAllDataForUser(_me);
            Console.WriteLine(_me.ToString());//benefit of toString override
        }

        #region List Menu
        void ViewMyLists()
        {
            var menuString = "\n1. Create empty list\n2. Back";
            while (true)
            {
                _userRepository.LoadAllDataForUser(_me);
                var index = 1;
                var lists = _me.Lists.Count == 0
                   ? "No lists."
                   : "Lists: \n" + Join("\n", _me.Lists.Select(x => (index++).ToString() + ". " + x.ToString()));
                Console.WriteLine(lists);
                ReadKey();
                var input = ReadLine(menuString);
                switch (input)
                {
                    case "1":
                        _executer.ExecuteMethod(new Func<bool, List>(CreateListFromInput), true);
                        ReadKey();
                        break;
                    case "2":
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        ReadKey();
                        break;
                }
            }
        }

        public List CreateListFromInput(bool x)
        {
            Console.WriteLine("Insert data to create a list: ");
            var created = new List {Id = Guid.NewGuid()};
            foreach (var prop in typeof(List).GetProperties())
            {
                if (prop.GetValue(created) != null) continue;// skip default value props
                Console.Write(prop.Name + ": ");
                var input = Console.ReadLine();
                prop.SetValue(created, Convert.ChangeType(input, prop.PropertyType), null);
            }
            created.Users.Add(_me);
            _listRepository.Create(created);
            return created;
        }
        #endregion

        #region Movie Menu
        void ViewAllMovies()
        {
            var menuString = "\n\n1. Create movie\n2. Select a movie by index\n3. Back";
            while (true)
            {
                var movies = _movieRepository.GetAll().ToList();
                int index = 1;
                var str = movies.Count == 0
                   ? "No lists.\n"
                   : "Movies: \n" + Join("\n", movies.Select(x => (index++).ToString() + ". " + x.ToString()));
                Console.WriteLine(str);
                ReadKey();
                var input = ReadLine(menuString);
                switch (input)
                {
                    case "1":
                        var created = _executer.ExecuteMethod(new Func<bool, Movie>(CreateMovieFromInput), true);
                        if (created == null)
                            Console.WriteLine("Cannot create movie. Please try again.");
                        break;
                    case "2":
                        var selected = _executer.ExecuteMethod(new Func<List<Movie>, Movie>(SelectMovie), movies);
                        if (selected == null)
                            Console.WriteLine("Movie not selected because of error.");
                        else
                        {
                            Console.WriteLine("Selected movie: " + _selectedMovie);
                            DisplayMovieMenu();
                        }
                        break;
                    case "3":
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        break;
                }
            }
        }

        private Movie SelectMovie(List<Movie> movies)
        {
            var input = ReadLine("Insert index (as in the list above):");
            int id;
            if(!int.TryParse(input, out id))
                throw  new Exception("Must insert a number");
            if(id < 1 || id > movies.Count)
                throw new Exception("Index must be inside the bounds of the list.");
            _selectedMovie = movies.ElementAt(id - 1);
            return _selectedMovie;
        }

        public Movie CreateMovieFromInput(bool x)
        {
            Console.WriteLine("Insert data to create a movie: ");
            var created = new Movie();
            created.Id = Guid.NewGuid();
            foreach (var prop in typeof(Movie).GetProperties())
            {
                if (prop.GetValue(created) != null && prop.Name != "ReleaseDate") continue;// skip default value props
                Console.Write(prop.Name + ": ");
                var input = Console.ReadLine();
                prop.SetValue(created, Convert.ChangeType(input, prop.PropertyType), null);
            }
            _movieRepository.Create(created);
            return created;
        }
        #endregion

        #region Movie Submenu
        void DisplayMovieMenu()
        {
            var menuString = "\n1. Add rating\n2. View ratings\n3. Add comment\n4. View comments\n5. Add to list\n6. Back";
            while (true)
            {
                ReadKey();
                var input = ReadLine(menuString);
                switch (input)
                {
                    case "1":
                        var created = _executer.ExecuteMethod(new Func<bool, Rating>(CreateRatingFromInput), true);
                        if (created == null)
                            Console.WriteLine("Cannot create rating. Please try again.");
                        break;
                    case "2":
                        _movieRepository.LoadRatingsForMovie(_selectedMovie);
                        var str = _selectedMovie.Ratings.Count == 0
                            ? "No ratings."
                            : Join("\n", _selectedMovie.Ratings);
                        Console.WriteLine(str);
                        break;
                    case "3":
                        created = _executer.ExecuteMethod(new Func<bool, Comment>(CreateCommentFromInput), true);
                        if (created == null)
                            Console.WriteLine("Cannot create comment. Please try again.");
                        break;
                    case "4":
                        _movieRepository.LoadCommentsForMovie(_selectedMovie);
                        str = _selectedMovie.Comments.Count == 0
                            ? "No comments."
                            : Join("\n------------------------\n", _selectedMovie.Comments);
                        Console.WriteLine(str);
                        break;
                    case "5":
                        var addedIntoList = _executer.ExecuteMethod(new Func<bool, List>(AddMovieToListRoutine), true);
                        if (addedIntoList == null)
                            Console.WriteLine("Movie not added to list because of error.");
                        else
                        {
                            Console.WriteLine($"Movie {_selectedMovie} added to list {addedIntoList}.");
                            DisplayMovieMenu();
                        }
                        break;
                    case "6":
                        return;
                    default:
                        Console.WriteLine("Invalid input.");
                        break;
                }
            }
        }

        public Rating CreateRatingFromInput(bool x)
        {
            //should check for existance
            Console.WriteLine("Insert data to create a rating: ");
            var created = new Rating();
            created.Id = Guid.NewGuid();
            created.Value = int.Parse(ReadLine("Value: "));

            created.UserId = _me.Id;
            created.MovieId = _selectedMovie.Id;
            _ratingRepository.Create(created);
            return created;
        }

        public Comment CreateCommentFromInput(bool x)
        {
            //should check for existance
            Console.WriteLine("Insert data to create a comment: ");
            var created = new Comment();
            created.Id = Guid.NewGuid();
            created.Value = ReadLine("Value: ");

            created.UserId = _me.Id;
            created.MovieId = _selectedMovie.Id;
            _commentRepository.Create(created);
            return created;
        }

        private List AddMovieToListRoutine(bool intro)
        {
            if(!_context.Entry(_me).Collection(u => u.Lists).IsLoaded)
                _context.Entry(_me).Collection(u => u.Lists).Load();

            if(_me.Lists.Count == 0)
                throw new Exception("You have no lists. Go to the list menu and create one.");

            var menuString = "\nInsert index of the list to Add Movie To (as in the list above):";
            var index = 1;
            var str = "Lists: \n" + Join("\n", _me.Lists.Select(x => (index++).ToString() + ". " + x.ToString())) + menuString;
            var input = ReadLine(str);

            int id;
            if (!int.TryParse(input, out id))
                throw new Exception("Must insert a number");
            if (id < 1 || id > _me.Lists.Count)
                throw new Exception("Index must be inside the bounds of the list.");

            var selectedList = _me.Lists.ElementAt(id - 1);
            selectedList.Movies.Add(_selectedMovie);
            _listRepository.Update(selectedList);

            return selectedList;
        }
        #endregion

#endregion
    }
}
