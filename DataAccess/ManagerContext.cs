﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DataAccess.Models;

namespace DataAccess
{
    public class ManagerContext: DbContext, IManagerContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Friendship> Friendships { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<List> Lists { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Action> Actions { get; set; }

        public ManagerContext()
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // define other constraints
            modelBuilder.Entity<Movie>().Property(m => m.RatingAvgCritics).HasPrecision(2, 1);
            modelBuilder.Entity<Movie>().Property(m => m.RatingAvgUsers).HasPrecision(2, 1);
            modelBuilder.Entity<List>().Property(l => l.RatingAvgUsers).HasPrecision(2, 1);

            // disable on delete cascade to avoid cycles
            //modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            base.OnModelCreating(modelBuilder);
        }
    }
}
