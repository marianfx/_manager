﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DataAccess.Models;
using Action = DataAccess.Models.Action;

namespace DataAccess
{
    public interface IManagerContext
    {

        DbSet<User> Users { get; set; }
        DbSet<Friendship> Friendships { get; set; }
        DbSet<Movie> Movies { get; set; }
        DbSet<Tag> Tags { get; set; }
        DbSet<List> Lists { get; set; }
        DbSet<Rating> Ratings { get; set; }
        DbSet<Comment> Comments { get; set; }
        DbSet<Action> Actions { get; set; }

        Database Database { get;}

        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();
    }
}
