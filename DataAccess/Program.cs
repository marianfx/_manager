﻿using System;
using DataAccess.Repositories;
using DataAccess.Repositories.Interfaces;
using DataAccess.Sample;
using Utilities;
using Utilities.Logger;

namespace DataAccess
{
    public class Program
    {
        public static void Main(string[] args)
        {
            IManagerContext context = new ManagerContext();
            var executer = new Executer(new ConsoleLogger());
            IUserRepository userRepository = new UserRepository(context);
            IMovieRepository movieRepository = new MovieRepository(context);
            IListRepository listRepository = new ListRepository(context);
            ICommentRepository commentRepository = new CommentRepository(context);
            IRatingRepository ratingRepository = new RatingRepository(context);

            //var db = new DatabaseInitializer(context, userRepository, movieRepository, listRepository, commentRepository, executer);
            //db.ExecuteAll();

            var cm = new ConsoleManager(context, userRepository, movieRepository, listRepository, commentRepository, ratingRepository, executer);
            cm.Execute();

            Console.ReadKey();
        }
    }
}
