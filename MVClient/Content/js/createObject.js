﻿$(document)
	.ready(function () {

	    $("#submitBtn")
			.click(callServer);
	});

var nextUrl = "";

function callServer() {
    var inputs = $(".row input");
    var data = {};
    for (var index = 0; index < inputs.length; index++) {
        var input = inputs[index];
        var name = $(input).attr("name");
        var type = $(input).attr("type");
        if (type == "checkbox")
            data[name] = $(input).is(":checked");
        else
            data[name] = $(input).val();

        console.log(name + " " + type);
    }
    console.log(data);

    var url = $("#callbackurl").text();
    var postForm = { //Fetch form data
        formData: data
    };

    $.ajax({
        type: "POST", // type of request
        url: url, //path of the request
        data: postForm,
        contentType: "application/x-www-form-urlencoded;odata=verbose", // data content type (header)

        // the function called on Success (no error returned by the server)
        success: function (result) {
            var error = result["error"];
            nextUrl = result["next"];
            console.log(nextUrl);
            if (result.status === 0) {
                console.log(error);
                displaySwal("Error happened", error, "warning");
            } else {
                displaySwal("Succes", "Redirecting you now", "success");
            }
        },
        // the function called on error (error returned from server or TimeOut Expired)
        error: function (err) {

            var errorText = "Unknown error happened (maybe timeout?).";
            if (err.hasOwnProperty("responseText")) {
                var response = JSON.parse(err.responseText);
                if (response.hasOwnProperty("message")) {
                    errorText = response.message;
                } else if (err.hasOwnProperty("statusText")) {
                    errorText = response.statusText;
                } else {
                    errorText = response.toString();
                }
            }
            console.log(err);
            console.log(errorText);
            return displaySwal("Error happened", errorText, "error");
        },
        timeout: 100000 // the time limit to wait for a response from the server, milliseconds
    });
}


function displaySwal(title, text, type, imageUrl) {

    var swalConfig = {
        title: title,
        text: text,
        showConfirmButton: false,
        showCancelButton: false,
        showLoaderOnConfirm: false,
        html: true
    };

    if (type) {
        swalConfig.type = type;
    }

    if (imageUrl) {
        swalConfig.imageUrl = imageUrl;
        swalConfig.imageSize = '180x180';
    }

    swal(swalConfig);
    setTimeout(function () {
        swal.close();
        if (nextUrl != null && nextUrl !== "")
            window.location.href = nextUrl;
    }, 3500);
}