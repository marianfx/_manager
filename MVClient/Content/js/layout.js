﻿var MAX_NR_WALLPAPERS = 7;
/**
 * Returns a random number between min (inclusive) and max (exclusive)
 * http://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
 */
function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

function AutoChangeWallpaper() {
    var current = Math.round(getRandomArbitrary(1, MAX_NR_WALLPAPERS));
    console.log(current);
    var value = "background: url('/Content/images/" + current + ".jpg') no-repeat center center fixed !important;" + 
                "-webkit-background-size: cover !important;" +
                "-moz-background-size: cover !important;" +
                "-o-background-size: cover !important;" +
                "background-size: cover !important;";
    var style = "<style>body{" + value + "} body::before{" + value + "}</style>";
    $(style).appendTo("head");
    //$("body.over").attr("background", "url('../images/" + current + ".jpg') no-repeat center center fixed !important;");
}

$(document).ready(function()
{
    AutoChangeWallpaper();
});
