﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using DataAccess.Models.Base;
using MVClient.Models;
using Newtonsoft.Json.Linq;
using System.Runtime.Serialization.Json;
using System.Security.Policy;
using System.Threading.Tasks;
using Utilities;
using Utilities.Logger;

namespace MVClient.Utils
{
    public static class Extensions
    {
        private static string BaseUrl = "http://localhost:6996/api";
        private static Executer _executer = new Executer(new ConsoleLogger());

        public static string SplitByCapitalLetters(this string s)
        {
            var r = new Regex(@"
                (?<=[A-Z])(?=[A-Z][a-z]) |
                 (?<=[^A-Z])(?=[A-Z]) |
                 (?<=[A-Za-z])(?=[^A-Za-z])", RegexOptions.IgnorePatternWhitespace);

            
            return r.Replace(s, " ");
        }

        public static string GetValidationError<T>(T theObject)
             where T : class, new()
        {
            var list = new List<ObjectDetail>();
            var created = new T();
            try
            {
                foreach (var prop in typeof(T).GetProperties())
                {
                    if (!((prop.GetValue(created) != null && prop.Name != "ReleaseDate" && prop.Name != "Value") || prop.PropertyType.BaseType == typeof(ModelBase) || (prop.Name.Contains("Id") && prop.Name != "ImdbId")))
                        if (prop.GetValue(theObject) == null)
                            return "The field " + prop.Name.SplitByCapitalLetters() + " must have a value.";

                }
            }
            catch
            {
                return "Invalid data provided.";
            }

            return null;
        }

        public static IEnumerable<ObjectDetail> CreateFieldsForObject<T>()
              where T : class, new()
        {
            var list = new List<ObjectDetail>();
            var created = new T();
            try
            {
                foreach (var prop in typeof(T).GetProperties())
                {
                    if ((prop.GetValue(created) != null && prop.Name != "ReleaseDate" && prop.Name != "Value") || prop.PropertyType.BaseType == typeof(ModelBase) || (prop.Name.Contains("Id") && prop.Name != "ImdbId")) continue;// skip default value props (special case release date for movie)
                    var objectDetail = new ObjectDetail(prop);
                    list.Add(objectDetail);
                }
            }
            catch
            {
                return new List<ObjectDetail>();
            }

            return list.AsEnumerable();
        }

        public static ObjectViewList CreateViewFieldsForObject<T>(T source)
              where T : class, new()
        {
            var list = new ObjectViewList
            {
                Name = typeof(T).Name.SplitByCapitalLetters()
            };
            try
            {
                foreach (var prop in typeof(T).GetProperties())
            {
                if (prop.Name.ToLower().EndsWith("id") || prop.Name.ToLower().Contains("password") ||
                    prop.PropertyType.BaseType == typeof(ModelBase)) // here mark as unchanged the models
                    continue;

                if (prop.PropertyType != typeof(string) && prop.PropertyType.GetInterface("IEnumerable") != null)
                    continue;

                var value = prop.GetValue(source) == null ? "No data." : prop.GetValue(source).ToString();

                var listElement = new ObjectViewListElement(prop, value);
                list.Add(listElement);
            }
            }
            catch
            {
                return new ObjectViewList();
            }

            return list;
        }

        public static ObjectViewList CreateViewFieldsForList<T>(IEnumerable<T> source)
            where T : class, new()
        {
            var name = typeof(T).Name.SplitByCapitalLetters();
            var list = new ObjectViewList
            {
                Name = name + (name.EndsWith("s") ? "" : "s")
            };
            try
            {
                var lst = (from ie in source
                           select ie).ToList();

                if (lst.Count == 0)
                    list.Add(ObjectViewListElement.EmptyElement);
                else
                {
                    name = name.ToLower();
                    for (var i = 1; i <= lst.Count; i++)
                    {
                        var elem = lst[i - 1] as ModelBase;
                        var typeName = typeof(T).Name;
                        var valToAdd = lst[i - 1].ToString().Contains("\n") ? lst[i - 1].ToString().Split('\n')[0] : lst[i - 1].ToString();
                        list.Add(new ObjectViewListElement
                        {
                            Name = "#" + i,
                            Value = $"<a href='/{typeName}/Get/{elem?.Id}'>{valToAdd}</a>",
                            Icon = name.Contains("user") ? "mdi-social-person" :name.Contains("list") ? "mdi-action-list" : name.Contains("movie") ? "mdi-maps-local-movies" : name.Contains("rating") ? "mdi-action-star-rate" : name.Contains("comment") ? "mdi-communication-comment" : "mdi -action-info-outline"
                        });
                    }
                }
            }
            catch(Exception e)
            {
                return new ObjectViewList();
            }

            return list;
        }

        /// <summary>
        /// Runs the method that reads an object data from the console, and then calls the service which creates the object in the database.
        /// </summary>
        /// <typeparam name="T">Represents a class (any, usually derived from ModelBase or an IEnumerable), from which objects can be created (hence the new() constraint)</typeparam>
        /// <param name="path">Represents the RELATIVE path of the API where the resource being created resides.</param>
        /// <returns></returns>
        public static T CreateObjectProcess<T>(string path, T created)
            where T : class, new()
        {
            try
            {
                var modelBase = created as ModelBase;
                if (modelBase != null && modelBase.Id == Guid.Empty)
                    modelBase.Id = Guid.NewGuid();
            }
            catch
            {
                // ignored
            }

            created = _executer.ExecuteMethod(
                new Func<string, string, bool, object, T>(DoWorkAndGetServerResponse<T>),
                path, "POST", true, created) as T;
            return created;
        }

        public static T UpdateObjectProocess<T>(string path, T updated)
            where T : class, new()
        {
            updated = _executer.ExecuteMethod(
                new Func<string, string, bool, object, T>(DoWorkAndGetServerResponse<T>),
                path, "PUT", true, updated) as T;
            return updated;
        }

        /// <summary>
        /// Runs the method that reads an object data from the console, and then calls the service which creates the object in the database.
        /// </summary>
        /// <typeparam name="T">Represents a class (any, usually derived from ModelBase or an IEnumerable), from which objects can be created (hence the new() constraint)</typeparam>
        /// <param name="path">Represents the RELATIVE path of the API where the resource being created resides.</param>
        /// <returns></returns>
        public static T GetObjectProcess<T>(string path)
            where T : class
        {
            return _executer.ExecuteMethod(
                new Func<string, string, bool, object, T>(DoWorkAndGetServerResponse<T>),
                path, "GET", false, null) as T;
        }
        
        /// <summary>
         /// Calls the WCF service on the specified path, with the specified method, uploads data if needed and gets the response of the server (throws Exception if the server returns an error code). It reads the response from the server as an ServerResponse object, and then, if it exists, deserializes the data fron the .Data component.
         /// </summary>
         /// <returns>The object returned from the serever in the "Data" component.</returns>
        public static T DoWorkAndGetServerResponse<T>(string path, string method = "GET", bool upData = false, object data = null)
            where T : class
        {
            var client = new WebClient();
            var serviceUrl = $"{BaseUrl}/{path}";
            byte[] responseData;

            if (upData)
            {
                client.Headers["Content-type"] = "text/plain";
                var ser = JObject.FromObject(data).ToString();
                var barr = Encoding.UTF8.GetBytes(ser);

                // invoke the REST method  
                responseData = client.UploadData(serviceUrl, method, barr);
            }
            else
            {
                responseData = client.DownloadData(serviceUrl);
            }

            var stream = new MemoryStream(responseData);
            var responseDeserializer = new DataContractJsonSerializer(typeof(ServerResponse));
            var response = responseDeserializer.ReadObject(stream) as ServerResponse;
            if (response == null || response.StatusCode == 0)
                throw new Exception("Error came from server: " + response?.Errors);

            var token = JToken.Parse(response.Data);
            var isArray = typeof(T) != typeof(string) && typeof(T).GetInterface("IEnumerable") != null;
            var output = isArray ? JArray.Parse(token.ToString()).ToObject<T>() : JObject.Parse(token.ToString()).ToObject(typeof(T));
            return output as T;
        }
    }
}