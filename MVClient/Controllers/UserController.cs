﻿using System;
using System.Linq;
using System.Web.Mvc;
using DataAccess.Models;
using MVClient.Models;
using MVClient.Utils;

namespace MVClient.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        [Authorize]
        public ActionResult Get(string id)
        {
            Guid myId;
            if(!Guid.TryParse(id, out myId))
                return Json(new {status = 0, data = "Cannot retrieve personal info. Inexistent user." }); // here do not display json but empty list cause it's direct request

            var me = Extensions.GetObjectProcess<User>($"/users/{myId}");
            var obj = new ObjectView
            {
                Id = me.Id.ToString(),
                Name = me.Username,
                Description = "View details about " + me.Username,
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var userData = Extensions.CreateViewFieldsForObject(me);
            var listData = Extensions.CreateViewFieldsForList(me.Lists);
            for (var i = 0; i < me.Ratings.Count; i++)
                me.Ratings.ElementAt(i).User = me;
            var ratingsData = Extensions.CreateViewFieldsForList(me.Ratings);
            for (var i = 0; i < me.Comments.Count; i++)
                me.Comments.ElementAt(i).User = me;
            var commentsData = Extensions.CreateViewFieldsForList(me.Comments);

            obj.Lists.Add(userData);
            obj.Lists.Add(listData);
            obj.Lists.Add(ratingsData);
            obj.Lists.Add(commentsData);
            return View("DisplayObject", obj);
        }
    }
}