﻿using System;
using System.Web.Mvc;
using DataAccess.Models;
using MVClient.Models;
using MVClient.Utils;

namespace MVClient.Controllers
{
    public class RatingController : Controller
    {
        // GET: Rating
        public ActionResult Get(string id)
        {
            Guid myId;
            if (!Guid.TryParse(id, out myId))
                return Json(new { status = 0, data = "Cannot retrieve info. Inexistent rating." }); // here do not display json but empty list cause it's direct request

            var res = Extensions.GetObjectProcess<Rating>($"/ratings/{myId}");
            var obj = new ObjectView
            {
                Id = res.Id.ToString(),
                Name = "Rating",
                Description = "A Rating from " + res.User.Username,
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var data = Extensions.CreateViewFieldsForObject(res);

            obj.Lists.Add(data);
            return View("DisplayObject", obj);
        }
    }
}