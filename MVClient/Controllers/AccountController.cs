﻿using System;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DataAccess.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using MVClient.Models;
using MVClient.Utils;

namespace MVClient.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        #region Managers and Constructor
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        #endregion

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Login(LoginViewModel formData)
        {
            if (formData == null)
            {
                return Json(new { status = 0, error = "Invalid input." });
            }

            formData.RememberMe = true;
            var err = Extensions.GetValidationError(formData);
            if (err != null)
            {
                return Json(new { status = 0, error = err });
            }

            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(formData.Email, formData.Password, formData.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return Json(new { status = 1, next = Url.Action("Index", "Home") });
                default:
                    return Json(new { status = 0, error = "Invalid login attempt." });
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            var model = new ObjectModel
            {
                CallbackUrl = "/Account/Register",
                Description = "Create a new User",
                SubmitText = "Register",
                Fields = Extensions.CreateFieldsForObject<User>()
            };
            return View("CreateObject", model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Register(User formData)
        {
            if (formData == null)
            {
                return Json(new { status = 0, error = "Invalid input."});
            }

            var err = Extensions.GetValidationError(formData);
            if (err != null)
            {
                return Json(new { status = 0, error = err});
            }

            try
            {
                // add to userlist
                var user = new ApplicationUser
                {
                    UserName = formData.Username,
                    Email = formData.Email
                };
                var result = UserManager.Create(user, formData.Password);
                if (!result.Succeeded)
                    throw new Exception(string.Join(".<br/>", result.Errors));

                // add to my Db
                formData.Id = Guid.Parse(user.Id);
                var usr = Extensions.CreateObjectProcess("/users/", formData);
                if (usr == null)
                {
                    UserManager.Delete(user);
                    throw new Exception("Cannot save user data. Try again later.");
                }
            }
            catch (Exception e)
            {
                return Json(new {status = 0, error = "Cannot create user. Error happened. Details:<br/>" + e.Message + "."});
            }

            return Json(new {status = 1, next = Url.Action("Login", "Account")});
        }

        //
        //
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}