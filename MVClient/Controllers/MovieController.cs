﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using DataAccess.Models;
using Microsoft.AspNet.Identity;
using MVClient.Models;
using MVClient.Utils;

namespace MVClient.Controllers
{
    public class MovieController : Controller
    {
        #region Movie Info Part
        // GET: Movie
        [Authorize]
        public ActionResult Index()
        {
            var movies = Extensions.GetObjectProcess<IEnumerable<Movie>>($"/movies/");
            var obj = new ObjectView
            {
                Id = "",
                Name = "Movies",
                Description = $"View all existing movies",
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var moviedata = Extensions.CreateViewFieldsForList(movies);

            obj.Lists.Add(moviedata);
            return View("DisplayObject", obj);
        }

        // GET: Movie
        [Authorize]
        public ActionResult Get(string id)
        {
            Guid myId;
            if (!Guid.TryParse(id, out myId))
                return Json(new { status = 0, data = "Cannot retrieve info. Inexistent movie." }); // here do not display json but empty list cause it's direct request

            var movie = Extensions.GetObjectProcess<Movie>($"/movies/{myId}");
            var obj = new ObjectView
            {
                Id = movie.Id.ToString(),
                Name = movie.Name,
                Description = "View details about " + movie.Name,
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var moviedata = Extensions.CreateViewFieldsForObject(movie);

            obj.Lists.Add(moviedata);
            return View("DisplayObject", obj);
        }
        
        [Authorize]
        public ActionResult Create()
        {
            var model = new ObjectModel
            {
                CallbackUrl = "/Movie/Create",
                Description = "Create a new Movie",
                SubmitText = "Submit",
                Fields = Extensions.CreateFieldsForObject<Movie>()
            };
            return View("CreateObject", model);
        }


        [Authorize]
        [HttpPost]
        public ActionResult Create(Movie formData)
        {
            if (formData == null)
            {
                return Json(new { status = 0, error = "Invalid input." });
            }

            var err = Extensions.GetValidationError(formData);
            if (err != null)
            {
                return Json(new { status = 0, error = err });
            }
            try
            {
                var res = Extensions.CreateObjectProcess("/movies/", formData);
                if (res == null)
                {
                    throw new Exception("Cannot save movie data. Try again later.");
                }
            }
            catch (Exception e)
            {
                return Json(new { status = 0, error = "Cannot create movie. Error happened. Details:<br/>" + e.Message + "." });
            }

            return Json(new { status = 1, next = Url.Action("Index", "Movie") });
        }
        #endregion

        #region Ratings
        [Authorize]
        public ActionResult Ratings(string id)
        {
            Guid myId;
            if (!Guid.TryParse(id, out myId))
                return Json(new { status = 0, data = "Cannot retrieve info. Inexistent movie." }); // here do not display json but empty list cause it's direct request
            
            var movie = Extensions.GetObjectProcess<Movie>($"/movies/{id}/ratings");
            var obj = new ObjectView
            {
                Id = "",
                Name = "Ratings",
                Description = $"View ratings for {movie.Name}",
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var basicdata = Extensions.CreateViewFieldsForList(movie.Ratings);

            obj.Lists.Add(basicdata);
            return View("DisplayObject", obj);
        }

        [Authorize]
        public ActionResult Rating(string id)
        {
            var model = new ObjectModel
            {
                CallbackUrl = "/Movie/Rating/" + id,
                Description = "Add a new Rating",
                SubmitText = "Submit",
                Fields = Extensions.CreateFieldsForObject<Rating>()
            };
            return View("CreateObject", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Rating(string id, Rating formData)
        {
            if (formData == null)
            {
                return Json(new { status = 0, error = "Invalid input." });
            }

            var err = Extensions.GetValidationError(formData);
            if (err != null)
            {
                return Json(new { status = 0, error = err });
            }
            try
            {
                var movie = Extensions.GetObjectProcess<Movie>($"/movies/{id}");
                movie.Comments = null;
                movie.Lists = null;
                movie.Ratings = null;
                movie.Tags = null;
                formData.Movie = movie;

                var me = Extensions.GetObjectProcess<User>($"/users/{User.Identity.GetUserId()}");
                me.Lists = null;
                me.Comments = null;
                me.IncomingFriendships = null;
                me.Ratings = null;
                me.OutgoingFriendships = null;
                formData.User = me;
                var res = Extensions.CreateObjectProcess("/ratings/", formData);
                if (res == null)
                {
                    throw new Exception("Cannot save rating. Try again later.");
                }
            }
            catch (Exception e)
            {
                return Json(new { status = 0, error = "Cannot create rating. Error happened. Details:<br/>" + e.Message + "." });
            }

            return Json(new { status = 1, next = Url.Action("Ratings", "Movie", new {id = id}) });
        }
        #endregion


        #region Comments
        [Authorize]
        public ActionResult Comments(string id)
        {
            Guid myId;
            if (!Guid.TryParse(id, out myId))
                return Json(new { status = 0, data = "Cannot retrieve info. Inexistent movie." }); // here do not display json but empty list cause it's direct request

            var movie = Extensions.GetObjectProcess<Movie>($"/movies/{id}/comments");
            var obj = new ObjectView
            {
                Id = "",
                Name = "Comments",
                Description = $"View comments for {movie.Name}",
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var basicdata = Extensions.CreateViewFieldsForList(movie.Comments);

            obj.Lists.Add(basicdata);
            return View("DisplayObject", obj);
        }

        [Authorize]
        public ActionResult Comment(string id)
        {
            var model = new ObjectModel
            {
                CallbackUrl = "/Movie/Comment/" + id,
                Description = "Add a new Comment",
                SubmitText = "Submit",
                Fields = Extensions.CreateFieldsForObject<Comment>()
            };
            return View("CreateObject", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Comment(string id, Comment formData)
        {
            if (formData == null)
            {
                return Json(new { status = 0, error = "Invalid input." });
            }

            var err = Extensions.GetValidationError(formData);
            if (err != null)
            {
                return Json(new { status = 0, error = err });
            }
            try
            {
                var movie = Extensions.GetObjectProcess<Movie>($"/movies/{id}");
                movie.Comments = null;
                movie.Lists = null;
                movie.Ratings = null;
                movie.Tags = null;
                formData.Movie = movie;

                var me = Extensions.GetObjectProcess<User>($"/users/{User.Identity.GetUserId()}");
                me.Lists = null;
                me.Comments = null;
                me.IncomingFriendships = null;
                me.Ratings = null;
                me.OutgoingFriendships = null;
                formData.User = me;
                var res = Extensions.CreateObjectProcess("/comments/", formData);
                if (res == null)
                {
                    throw new Exception("Cannot save comment. Try again later.");
                }
            }
            catch (Exception e)
            {
                return Json(new { status = 0, error = "Cannot create comment. Error happened. Details:<br/>" + e.Message + "." });
            }

            return Json(new { status = 1, next = Url.Action("Comments", "Movie", new { id = id }) });
        }
        #endregion
    }
}