﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DataAccess.Models;
using Microsoft.AspNet.Identity;
using MVClient.Models;
using MVClient.Utils;

namespace MVClient.Controllers
{
    public class ListController : Controller
    {
        // GET: List
        [Authorize]
        public ActionResult Index()
        {
            var me = Extensions.GetObjectProcess<User>($"/users/{User.Identity.GetUserId()}");
            var obj = new ObjectView
            {
                Id = me.Id.ToString(),
                Name = $"{me.Username}'s lists",
                Description = $"View {me.Username}'s lists.",
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var listData = Extensions.CreateViewFieldsForList(me.Lists);
            
            obj.Lists.Add(listData);
            return View("DisplayObject", obj);
        }

        // GET: List
        [Authorize]
        public ActionResult Get(string id)
        {
            Guid myId;
            if (!Guid.TryParse(id, out myId))
                return Json(new { status = 0, data = "Cannot retrieve info. Inexistent list." }); // here do not display json but empty list cause it's direct request

            var list = Extensions.GetObjectProcess<List>($"/lists/{myId}");
            var obj = new ObjectView
            {
                Id = list.Id.ToString(),
                Name = list.Name,
                Description = "View details about " + list.Name,
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var listdata = Extensions.CreateViewFieldsForObject(list);
            var userData = Extensions.CreateViewFieldsForList(list.Users);
            var movieData = Extensions.CreateViewFieldsForList(list.Movies);

            obj.Lists.Add(listdata);
            obj.Lists.Add(userData);
            obj.Lists.Add(movieData);
            return View("DisplayObject", obj);
        }

        // GET: List/Create
        [Authorize]
        public ActionResult Create()
        {
            var model = new ObjectModel
            {
                CallbackUrl = "/List/Create",
                Description = "Create a new List",
                SubmitText = "Submit",
                Fields = Extensions.CreateFieldsForObject<List>()
            };
            return View("CreateObject", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Create(List formData)
        {
            if (formData == null)
            {
                return Json(new { status = 0, error = "Invalid input." });
            }

            var err = Extensions.GetValidationError(formData);
            if (err != null)
            {
                return Json(new { status = 0, error = err });
            }
            try
            {
                var me = Extensions.GetObjectProcess<User>($"/users/{User.Identity.GetUserId()}");
                me.Lists = null;
                me.Comments = null;
                me.IncomingFriendships = null;
                me.Ratings = null;
                me.OutgoingFriendships = null;
                formData.Users.Add(me);
                var lst = Extensions.CreateObjectProcess("/lists/", formData);
                if (lst == null)
                {
                    throw new Exception("Cannot save list data. Try again later.");
                }
            }
            catch (Exception e)
            {
                return Json(new { status = 0, error = "Cannot create list. Error happened. Details:<br/>" + e.Message + "." });
            }

            return Json(new { status = 1, next = Url.Action("Index", "List") });
        }

        [Authorize]
        public ActionResult AddToListDisplay(string id)
        {
            var me = Extensions.GetObjectProcess<User>($"/users/{User.Identity.GetUserId()}");
            var obj = new ObjectView
            {
                Id = me.Id.ToString(),
                Name = $"{me.Username}'s lists",
                Description = "Select a list to add the movie to",
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var listData = Extensions.CreateViewFieldsForList(me.Lists);
            for (var i = 0; i < listData.Elements.Count; i++)
            {
                listData.Elements[i].Name = me.Lists.ElementAt(i).Name;
                listData.Elements[i].Value = $"<a href='/List/AddToList?listId={me.Lists.ElementAt(i).Id}&movieId={id}'>Add here</a>";
            }

            obj.Lists.Add(listData);
            return View("DisplayObject", obj);
        }

        [Authorize]
        public ActionResult AddToList(string listId, string movieId)
        {
            try
            {
                var list = Extensions.GetObjectProcess<User>($"/users/{User.Identity.GetUserId()}").Lists.FirstOrDefault(l => l.Id == Guid.Parse(listId));
                var movie = Extensions.GetObjectProcess<Movie>($"/movies/{movieId}");
                //movie.Lists = null;
                //movie.Comments = null;
                //movie.Ratings = null;
                //movie.Tags = null;
                list?.Movies.Clear();
                list?.Movies.Add(movie);
                
                var lst = Extensions.DoWorkAndGetServerResponse<List>($"/lists/{listId}", "PUT", true, list); ;
                if (lst == null)
                {
                    throw new Exception("Cannot save list data. Try again later.");
                }
            }
            catch (Exception e)
            {
                return Json(new { status = 0, error = "Cannot create list. Error happened. Details:<br/>" + e.Message + "." });
            }

            return RedirectToAction("Get", "List", new {id = listId});
        }
    }
}