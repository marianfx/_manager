﻿using System;
using System.Web.Mvc;
using DataAccess.Models;
using MVClient.Models;
using MVClient.Utils;

namespace MVClient.Controllers
{
    public class CommentController : Controller
    {
        // GET: Comment
        public ActionResult Get(string id)
        {
            Guid myId;
            if (!Guid.TryParse(id, out myId))
                return Json(new { status = 0, data = "Cannot retrieve info. Inexistent comment." }); // here do not display json but empty list cause it's direct request

            var res = Extensions.GetObjectProcess<Comment>($"/comments/{myId}");
            var obj = new ObjectView
            {
                Id = res.Id.ToString(),
                Name = "Comment",
                Description = "A comment from " + res.User.Username,
                ImageUrl = Url.Content("~/Content/basic_disc.png")
            };
            var data = Extensions.CreateViewFieldsForObject(res);

            obj.Lists.Add(data);
            return View("DisplayObject", obj);
        }
    }
}