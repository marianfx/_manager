﻿namespace MVClient.Models
{
    public class ServerResponse
    {

        /// <summary>
        /// Status code to help the client determine the success / failure
        /// </summary>
        public int StatusCode { get; set; }

        /// <summary>
        /// List of error strings
        /// </summary>
        public string[] Errors { get; set; } = new string[1];

        /// <summary>
        /// JSON object representing the data to send to the client
        /// </summary>
        public string Data { get; set; } = string.Empty;


        public ServerResponse()
        {
        }
    }
}
