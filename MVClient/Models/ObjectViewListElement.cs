﻿using System;
using System.Reflection;
using MVClient.Utils;

namespace MVClient.Models
{
    public class ObjectViewListElement
    {
        private readonly Type _type;
        public string Name { get; set; }
        public string Value { get; set; }
        public string Icon { get; set; }

        public ObjectViewListElement()
        {
            
        }

        public ObjectViewListElement(PropertyInfo source, string value)
        {
            _type = source.PropertyType;
            Name = source.Name.SplitByCapitalLetters();
            SetUpValue(value);
            SetUpIconFromName();
        }

        private void SetUpIconFromName()
        {
            var tempName = Name.ToLower();
            Icon = tempName.Contains("password") ? "mdi-action-lock" : tempName.Contains("mail") ? "mdi-communication-email" : tempName.Contains("photo") || tempName.Contains("image") || tempName.Contains("picture") ? "mdi-image-camera-alt" : tempName.Contains("name") ? "mdi-social-person" : _type == typeof(bool) ? "mdi-action-help" : _type == typeof(DateTime) ? "mdi-av-timer" : _type == typeof(int) || _type == typeof(double) ? "mdi-communication-dialpad" : "mdi-action-info-outline";
        }

        private void SetUpValue(string value)
        {
            Value = value;
            if (Name.ToLower().Contains("url") || Name.ToLower().Contains("trailer"))
                if (Name.ToLower().Contains("photo") || Name.ToLower().Contains("image") || Name.ToLower().Contains("picture"))
                    Value = $"<img src='{value}' style='mad-width:50px; height:auto;'>";
                else
                    Value = $"<a href='{value}' target='_blank'>Click here</a>";
            else if (Name.ToLower().Contains("mail"))
                Value = $"<a href='mailto: {value}?Subject=Hi%20there' target='_top'>{value}</a>";
        }

        public static ObjectViewListElement EmptyElement => new ObjectViewListElement
        {
            Name = "Info",
            Value =  "No data.",
            Icon = "mdi-action-highlight-remove"
        };

        public override string ToString()
        {
            return Name + " - " + Value + " [" + _type.Name + "]";
        }
    }
}