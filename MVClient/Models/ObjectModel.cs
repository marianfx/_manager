﻿using System.Collections.Generic;

namespace MVClient.Models
{
    public class ObjectModel
    {
        public string CallbackUrl { get; set; }
        public string FormMethod { get; set; } = "post";
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<ObjectDetail> Fields { get; set; }
        public string SubmitText { get; set; } = "Submit";
    }
}