﻿using System;
using System.Reflection;
using MVClient.Utils;

namespace MVClient.Models
{
    public class ObjectDetail
    {
        private readonly Type _type;
        public string Name { get; set; }
        public string Type { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }

        public ObjectDetail(PropertyInfo source)
        {
            _type = source.PropertyType;
            Name = source.Name;
            Description = Name.SplitByCapitalLetters();
            SetUpIconFromName();
            SetUpTypeFromName();
        }

        private void SetUpIconFromName()
        {
            var tempName = Name.ToLower();
            Icon = tempName.Contains("password") ? "lock" : tempName.Contains("mail") ? "email" : tempName.Contains("name") ? "person_pin" : _type == typeof(bool) ? "live_help" : _type == typeof(DateTime) ? "av_timer" : _type == typeof(int) || _type == typeof(double) ? "dialpad" : "mode_edit";
        }

        private void SetUpTypeFromName()
        {
            var tempName = Name.ToLower();
            Type = tempName.Contains("password") ? "password" : _type == typeof(bool) ? "checkbox" : _type == typeof(DateTime) ? "date" : _type == typeof(int) || _type == typeof(double) ? "number" : "text";
        }
    }
}