﻿using System.Collections.Generic;

namespace MVClient.Models
{
    public class ObjectViewList
    {
        public string Name { get; set; }
        public List<ObjectViewListElement> Elements { get; set; }

        public ObjectViewList()
        {
            Elements = new List<ObjectViewListElement>();
        }

        public void Add(ObjectViewListElement element)
        {
            Elements.Add(element);
        }
    }
}