﻿using System.Collections.Generic;

namespace MVClient.Models
{
    public class ObjectView
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public List<ObjectViewList> Lists { get; set; }

        public ObjectView()
        {
            Lists = new List<ObjectViewList>();
        }

        public override string ToString()
        {
            return Name + " - " + Description;
        }
    }
}