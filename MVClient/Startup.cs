﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(MVClient.Startup))]
namespace MVClient
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
