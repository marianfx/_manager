﻿namespace ManagerApi
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._startBtn = new System.Windows.Forms.Button();
            this._stopBtn = new System.Windows.Forms.Button();
            this.logTb = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // _startBtn
            // 
            this._startBtn.Location = new System.Drawing.Point(13, 28);
            this._startBtn.Name = "_startBtn";
            this._startBtn.Size = new System.Drawing.Size(130, 46);
            this._startBtn.TabIndex = 0;
            this._startBtn.Text = "START";
            this._startBtn.UseVisualStyleBackColor = true;
            this._startBtn.Click += new System.EventHandler(this._startBtn_Click);
            // 
            // _stopBtn
            // 
            this._stopBtn.Location = new System.Drawing.Point(149, 28);
            this._stopBtn.Name = "_stopBtn";
            this._stopBtn.Size = new System.Drawing.Size(130, 46);
            this._stopBtn.TabIndex = 1;
            this._stopBtn.Text = "STOP";
            this._stopBtn.UseVisualStyleBackColor = true;
            this._stopBtn.Click += new System.EventHandler(this._stopBtn_Click);
            // 
            // logTb
            // 
            this.logTb.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logTb.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.logTb.ForeColor = System.Drawing.Color.Green;
            this.logTb.Location = new System.Drawing.Point(13, 80);
            this.logTb.Multiline = true;
            this.logTb.Name = "logTb";
            this.logTb.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.logTb.Size = new System.Drawing.Size(505, 161);
            this.logTb.TabIndex = 2;
            this.logTb.WordWrap = false;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 253);
            this.Controls.Add(this.logTb);
            this.Controls.Add(this._stopBtn);
            this.Controls.Add(this._startBtn);
            this.Name = "MainWindow";
            this.Text = "RESTFul WCF _Manager API";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button _startBtn;
        private System.Windows.Forms.Button _stopBtn;
        private System.Windows.Forms.TextBox logTb;
    }
}

