﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using ManagerApi.Interfaces;
using ManagerApi.Services;
using Utilities;

namespace ManagerApi
{
    public class Controller
    {
        private readonly List<ServiceHost> _hosts;
        private const string BaseAddress = @"http://localhost:6996/api";
        private readonly Executer _executer;

        public Controller(Executer executer)
        {
            _executer = executer;
            _hosts = new List<ServiceHost>();
        }

        public void RegisterAllServices()
        {
            // service will be accessed through 'BaseAddress/service/' and not 'BaseAddress/service' because WCF does not allow using the same service host base addr for multiple services
            RegisterService(typeof(UserService), typeof(IUserService), "/users");
            RegisterService(typeof(MovieService), typeof(IMovieService), "/movies");
            RegisterService(typeof(ListService), typeof(IListService), "/lists");
            RegisterService(typeof(RatingService), typeof(IRatingService), "/ratings");
            RegisterService(typeof(CommentService), typeof(ICommentService), "/comments");
            RegisterService(typeof(FriendshipService), typeof(IFriendshipService), "/friendships");
            RegisterService(typeof(ActionService), typeof(IActionService), "/actions");
            DisplayAllHosts();
        }

        private void DisplayAllHosts()
        {
            var logger = _executer.GetLogger();
            logger.Log("Available endpoints:");
            foreach (var host in _hosts)
            {
                foreach (var se in host.Description.Endpoints)
                {
                    var temp = se.Address.ToString();
                    logger.Log(temp);
                }
            }
        }

        public void RegisterService(Type serviceImplementation, Type serviceInterface, string path)
        {
            var host = new ServiceHost(serviceImplementation, new Uri(BaseAddress + path));
            
            var endpoint = host.AddServiceEndpoint(serviceInterface, new WebHttpBinding(), "");
            var beh = new WebHttpBehavior
            {
                AutomaticFormatSelectionEnabled = true,
                DefaultBodyStyle = WebMessageBodyStyle.Bare,
                DefaultOutgoingRequestFormat = WebMessageFormat.Json,
                DefaultOutgoingResponseFormat = WebMessageFormat.Json,
                FaultExceptionEnabled = false
            };
            endpoint.Behaviors.Add(beh);

            // set per-call behavior
            var serviceBeh = host.Description.Behaviors.Find<ServiceBehaviorAttribute>();
            serviceBeh.InstanceContextMode = InstanceContextMode.PerCall;
            serviceBeh.MaxItemsInObjectGraph = int.MaxValue;
            serviceBeh.IgnoreExtensionDataObject = true;
            
            _hosts.Add(host);
        }

        public void UnregisterAllServices()
        {
            _hosts.Clear();
        }

        public void StartServer()
        {
            _executer.ExecuteMethod(new Action(() =>
            {
                foreach (var serviceHost in _hosts)
                {
                    serviceHost.Open();
                }
            }));
        }

        public void StopServer()
        {
            _executer.ExecuteMethod(new Action(() =>
            {
                foreach (var serviceHost in _hosts)
                {
                    serviceHost.Close();
                }
            }));
        }
    }
}
