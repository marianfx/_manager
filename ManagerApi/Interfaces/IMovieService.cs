﻿using System.ServiceModel;
using System.ServiceModel.Web;
using ManagerApi.Models;

namespace ManagerApi.Interfaces
{
    [ServiceContract]
    public interface IMovieService: IService
    {
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/{id}/ratings")]
        ServerResponse GetMovieWithRatings(string id);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/{id}/comments")]
        ServerResponse GetMovieWithComments(string id);
    }
}
