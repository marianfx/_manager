﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using ManagerApi.Models;

namespace ManagerApi.Interfaces
{
    [ServiceContract]
    public interface IUserService : IService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare,
            UriTemplate = "/auth")]
        ServerResponse CheckForUser(Stream data);
    }
}
