﻿using DataAccess;
using DataAccess.Models;
using DataAccess.Repositories;
using ManagerApi.Interfaces;

namespace ManagerApi.Services
{
    public class CommentService : BaseCrudService<Comment>, ICommentService
    {
        public CommentService()
        {
            Repository = new CommentRepository(new ManagerContext());
        }
    }
}
