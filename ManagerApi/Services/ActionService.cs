﻿using DataAccess;
using DataAccess.Models;
using DataAccess.Repositories;
using ManagerApi.Interfaces;

namespace ManagerApi.Services
{
    public class ActionService : BaseCrudService<Action>, IActionService
    {
        public ActionService()
        {
            Repository = new ActionRepository(new ManagerContext());
        }
    }
}
