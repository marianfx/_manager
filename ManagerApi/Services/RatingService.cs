﻿using DataAccess;
using DataAccess.Models;
using DataAccess.Repositories;
using ManagerApi.Interfaces;

namespace ManagerApi.Services
{
    public class RatingService : BaseCrudService<Rating>, IRatingService
    {
        public RatingService()
        {
            Repository = new RatingRepository(new ManagerContext());
        }
    }
}
