﻿using DataAccess;
using DataAccess.Models;
using DataAccess.Repositories;
using ManagerApi.Interfaces;

namespace ManagerApi.Services
{
    public class FriendshipService : BaseCrudService<Friendship>, IFriendshipService
    {
        public FriendshipService()
        {
            Repository = new FriendshipRepository(new ManagerContext());
        }
    }
}
