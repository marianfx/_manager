﻿using System;
using System.IO;
using System.ServiceModel.Web;
using CryptSharp;
using DataAccess;
using DataAccess.Models;
using DataAccess.Repositories;
using ManagerApi.Interfaces;
using ManagerApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utilities;

namespace ManagerApi.Services
{
    public class UserService: BaseCrudService<User>, IUserService
    {
        public UserService()
        {
            Repository = new UserRepository(new ManagerContext());
        }

        /// <summary>
        /// Override the default Create method to insert the hashed password.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public override ServerResponse Create(Stream data)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var obj = new User();
                Statics.GetJsonObjectFromStream(data, ref obj);
                if(obj.Id == Guid.Empty)
                    obj.Id = Guid.NewGuid();
                obj.Password = Crypter.Blowfish.Crypt(obj.Password);

                Repository.Create(obj);
                response.Data = JObject.FromObject(obj, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendCreated();
            }
            catch (Exception e)
            {
                if (e.Source != null && e.Source == "EntityFramework")
                    return response.SendConflict(Statics.GetDetailedError(e));

                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }

        public ServerResponse CheckForUser(Stream data)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var user = new User();
                Statics.GetJsonObjectFromStream(data, ref user, false);
                if(user.Username == null || user.Password == null)
                    return response.SendBadRequest("You must provide username and password.");

                var userRepo = Repository as UserRepository;
                var existingUser = userRepo?.GetByUsername(user.Username);
                if(existingUser == null)
                    return response.SendUnauthorized();

                var cryptedPass = existingUser.Password;
                var isPassOk = Crypter.CheckPassword(user.Password, cryptedPass);
                if (!isPassOk)
                    return response.SendUnauthorized();

                existingUser.Password = null;// remove pass from client sent msg
                response.Data = JObject.FromObject(existingUser, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }
        
        public override ServerResponse GetById(string id)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var guid = Statics.ParseGuidWithWebEx(id);
                var obj = Repository.GetById(guid);
                var userRepo = Repository as UserRepository;
                userRepo?.LoadAllDataForUser(obj);
                if (obj == null)
                    return response.SendNoContent();

                response.Data = JObject.FromObject(obj, new JsonSerializer {ReferenceLoopHandling = ReferenceLoopHandling.Ignore}).ToString();
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }
    }
}
