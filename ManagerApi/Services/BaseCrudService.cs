﻿using System;
using System.IO;
using System.Linq;
using System.ServiceModel.Web;
using DataAccess.Models.Base;
using DataAccess.Repositories.Interfaces;
using ManagerApi.Interfaces;
using ManagerApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utilities;

namespace ManagerApi.Services
{
    public class BaseCrudService<T>: IService
        where T: ModelBase, new()
    {
        /// <summary>
        /// Need to instantiate this for each of the derived classes, in the parameterless constructor (cause WCF uses it to build the service for each call, and it must have the appropriate repository).
        /// </summary>
        protected IRepository<T> Repository;

        public virtual ServerResponse Create(Stream data)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var obj = new T();
                Statics.GetJsonObjectFromStream(data, ref obj);
                obj.Id = Guid.NewGuid();

                Repository.Create(obj);
                response.Data = JObject.FromObject(obj, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendCreated();
            }
            catch (Exception e)
            {
                if (e.Source != null && e.Source == "EntityFramework")
                    return response.SendConflict(Statics.GetDetailedError(e));

                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }

        public virtual ServerResponse Delete(string id)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var guid = Statics.ParseGuidWithWebEx(id);
                var obj = Repository.GetById(guid);
                if (obj == null)
                    return response.SendNoContent();

                Repository.Delete(obj);
                response.Data = JObject.FromObject(obj, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }

        public virtual ServerResponse GetAll()
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var objs = Repository.GetAll().ToArray();
                response.Data = JArray.FromObject(objs, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString(Formatting.Indented);
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendServerError(Statics.GetDetailedError(e));
            }
        }

        public virtual ServerResponse GetById(string id)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var guid = Statics.ParseGuidWithWebEx(id);
                var obj = Repository.GetById(guid);
                if (obj == null)
                    return response.SendNoContent();

                response.Data = JObject.FromObject(obj, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }

        public virtual ServerResponse Update(string id, Stream data)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var guid = Statics.ParseGuidWithWebEx(id);
                var obj = Repository.GetById(guid);
                if (obj == null)
                    return response.SendNoContent();

                Statics.GetJsonObjectFromStream(data, ref obj, false);
                Repository.Update(obj);
                response.Data = JObject.FromObject(obj, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }
    }
}
