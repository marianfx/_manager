﻿using System;
using System.ServiceModel.Web;
using DataAccess;
using DataAccess.Models;
using DataAccess.Repositories;
using ManagerApi.Interfaces;
using ManagerApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utilities;

namespace ManagerApi.Services
{
    public class MovieService: BaseCrudService<Movie>, IMovieService
    {
        public MovieService()
        {
            Repository = new MovieRepository(new ManagerContext());
        }

        public ServerResponse GetMovieWithRatings(string id)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var guid = Statics.ParseGuidWithWebEx(id);
                var obj = Repository.GetById(guid);
                (Repository as MovieRepository)?.LoadRatingsForMovie(obj);
                if (obj == null)
                    return response.SendNoContent();

                response.Data = JObject.FromObject(obj, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }
        
        public ServerResponse GetMovieWithComments(string id)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var guid = Statics.ParseGuidWithWebEx(id);
                var obj = Repository.GetById(guid);
                (Repository as MovieRepository)?.LoadCommentsForMovie(obj);
                if (obj == null)
                    return response.SendNoContent();

                response.Data = JObject.FromObject(obj, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }
    }
}
