﻿using System;
using System.ServiceModel.Web;
using DataAccess;
using DataAccess.Models;
using DataAccess.Repositories;
using ManagerApi.Interfaces;
using ManagerApi.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Utilities;

namespace ManagerApi.Services
{
    public class ListService : BaseCrudService<List>, IListService
    {
        public ListService()
        {
            Repository = new ListRepository(new ManagerContext());
        }

        public override ServerResponse GetById(string id)
        {
            var response = new ServerResponse(WebOperationContext.Current);
            try
            {
                var guid = Statics.ParseGuidWithWebEx(id);
                var obj = Repository.GetById(guid);
                var userRepo = Repository as ListRepository;
                userRepo?.LoadAllDataForList(obj);
                if (obj == null)
                    return response.SendNoContent();

                response.Data = JObject.FromObject(obj, new JsonSerializer { ReferenceLoopHandling = ReferenceLoopHandling.Ignore }).ToString();
                return response.SendOk();
            }
            catch (Exception e)
            {
                return response.SendBadRequest(Statics.GetDetailedError(e));
            }
        }
    }
}
