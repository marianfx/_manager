﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Windows.Forms;
using ManagerApi.Interfaces;
using ManagerApi.Services;
using Utilities;
using Utilities.Logger;

namespace ManagerApi
{
    public partial class MainWindow : Form
    {
        private bool _switch = true;
        private readonly ILogger _logger;
        private readonly Executer _executer;
        private readonly Controller _controller;

        public MainWindow()
        {
            InitializeComponent();

            _logger = new TextboxLogger(this.logTb);
            _executer = new Executer(_logger);
            _controller = new Controller(_executer);

            SwitchServerStatus();
        }

        private void _startBtn_Click(object sender, EventArgs e)
        {
            _controller.RegisterAllServices();
            SwitchServerStatus();
        }

        private void _stopBtn_Click(object sender, EventArgs e)
        {
            SwitchServerStatus();
            _controller.UnregisterAllServices();
        }

        private void SwitchServerStatus()
        {
            if(_switch)
                _controller.StopServer();
            else
                _controller.StartServer();

            _startBtn.Enabled = _switch;
            _stopBtn.Enabled = !_switch;
            _switch = !_switch;
        }
    }
}
