﻿using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel.Web;

namespace ManagerApi.Models
{
    [DataContract]
    public class ServerResponse
    {
        private readonly WebOperationContext _webContext;

        /// <summary>
        /// Status code to help the client determine the success / failure
        /// </summary>
        [DataMember]
        public int StatusCode { get; set; }

        /// <summary>
        /// List of error strings
        /// </summary>
        [DataMember]
        public string[] Errors { get; set; } = new string[1];

        /// <summary>
        /// JSON object representing the data to send to the client
        /// </summary>
        [DataMember]
        public string Data { get; set; } = string.Empty;


        public ServerResponse(WebOperationContext webContext)
        {
            _webContext = webContext;
        }

        public ServerResponse SendCreated()
        {
            StatusCode = 1;
            if (_webContext != null)
                _webContext.OutgoingResponse.StatusCode = HttpStatusCode.Created;

            return this;
        }

        public ServerResponse SendOk()
        {
            StatusCode = 1;
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.OK;

            return this;
        }

        /// <summary>
        /// Must contain no body.
        /// </summary>
        /// <returns></returns>
        public ServerResponse SendNoContent()
        {
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NoContent;

            return this;
        }
        
        public ServerResponse SendConflict(string details = "")
        {
            StatusCode = 0;
            Errors = new[] { "Conflict occurred, please change your request." + details };
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Conflict;

            return this;
        }

        public ServerResponse SendBadRequest(string details = "None.")
        {
            StatusCode = 0;
            Errors = new[] { "You've done such a bad request, oh dear. Details: " + details };
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;

            return this;
        }

        public ServerResponse SendServerError(string details = "None.")
        {
            StatusCode = 0;
            Errors = new[] { "The server has gotten himself into some issues. Details: " + details };
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.InternalServerError;

            return this;
        }

        public ServerResponse SendUnauthorized()
        {
            StatusCode = 0;
            Errors = new[] { "There is no username matching that user and password."};
            if (WebOperationContext.Current != null)
                WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Unauthorized;

            return this;
        }
    }
}
